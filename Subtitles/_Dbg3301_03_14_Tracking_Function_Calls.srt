1
00:00:01,800 --> 00:00:07,020
another important command here is

2
00:00:04,400 --> 00:00:09,900
tracking function calls

3
00:00:07,020 --> 00:00:12,780
and this is this is a good starting

4
00:00:09,900 --> 00:00:16,340
point for debugging processes for

5
00:00:12,780 --> 00:00:19,380
example if you want to uh debug some

6
00:00:16,340 --> 00:00:22,800
user mode applications we don't know

7
00:00:19,380 --> 00:00:26,279
where to start we don't know how to uh

8
00:00:22,800 --> 00:00:28,019
examine the target application we could

9
00:00:26,279 --> 00:00:31,439
use uh

10
00:00:28,019 --> 00:00:34,200
this track command we will see some

11
00:00:31,439 --> 00:00:36,059
examples this is a really helpful

12
00:00:34,200 --> 00:00:38,280
command in case you want to reverse for

13
00:00:36,059 --> 00:00:41,640
example some of the internals of Windows

14
00:00:38,280 --> 00:00:43,980
to see how how they work internally this

15
00:00:41,640 --> 00:00:46,260
is a really good starting point we

16
00:00:43,980 --> 00:00:48,420
should and it's highly recommended to

17
00:00:46,260 --> 00:00:50,879
configure the symbol files before using

18
00:00:48,420 --> 00:00:53,640
this command because it gets it gives

19
00:00:50,879 --> 00:00:57,180
you a better results the function names

20
00:00:53,640 --> 00:00:59,579
it tries to resolve the function names

21
00:00:57,180 --> 00:01:01,160
and find the function names and if you

22
00:00:59,579 --> 00:01:03,780
don't use the

23
00:01:01,160 --> 00:01:07,799
symbol server then it just gives you

24
00:01:03,780 --> 00:01:11,159
some addresses commands and it uses the

25
00:01:07,799 --> 00:01:13,200
same mechanism as instrumentation that's

26
00:01:11,159 --> 00:01:16,799
the pin that I previously talked about

27
00:01:13,200 --> 00:01:18,799
it and just traces for calls call

28
00:01:16,799 --> 00:01:22,080
instructions and rate instruction

29
00:01:18,799 --> 00:01:24,540
recursively on the application will be

30
00:01:22,080 --> 00:01:26,280
executed and the application will get a

31
00:01:24,540 --> 00:01:29,880
chance to get executed in the target

32
00:01:26,280 --> 00:01:31,740
system the first and three we will see

33
00:01:29,880 --> 00:01:34,259
some example of the first interview is

34
00:01:31,740 --> 00:01:37,380
the calling address the where where it's

35
00:01:34,259 --> 00:01:39,060
called and the second address is the

36
00:01:37,380 --> 00:01:42,979
return where the function is actually

37
00:01:39,060 --> 00:01:45,780
returned we can specify number of

38
00:01:42,979 --> 00:01:48,000
instructions to get to get executed in

39
00:01:45,780 --> 00:01:50,640
the target debuggee or we could use the

40
00:01:48,000 --> 00:01:53,280
default uh behavior after this this

41
00:01:50,640 --> 00:01:57,920
command this is the view of the track

42
00:01:53,280 --> 00:02:01,860
command and as you can see it Maps the

43
00:01:57,920 --> 00:02:06,060
function addresses to the corresponding

44
00:02:01,860 --> 00:02:10,700
names and it gets it gets the names from

45
00:02:06,060 --> 00:02:13,860
the symbol pdb files and it's also

46
00:02:10,700 --> 00:02:17,160
worthy to note that the track command is

47
00:02:13,860 --> 00:02:19,500
able to trace function calls starting

48
00:02:17,160 --> 00:02:22,379
from user mode to kernel mode and kernel

49
00:02:19,500 --> 00:02:26,599
mode user mode just like the

50
00:02:22,379 --> 00:02:26,599
instrumentation stepping or icon

