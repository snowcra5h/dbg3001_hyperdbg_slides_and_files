1
00:00:00,020 --> 00:00:06,680
no let's see uh how we can hook a cpuid

2
00:00:04,880 --> 00:00:10,440
instructions

3
00:00:06,680 --> 00:00:13,320
uh basically uh like all the other

4
00:00:10,440 --> 00:00:16,800
events cpuid hooking is also another

5
00:00:13,320 --> 00:00:19,140
event and is there's a part it's

6
00:00:16,800 --> 00:00:22,140
available as a part of hypervised their

7
00:00:19,140 --> 00:00:27,000
introspection exports and Hyper division

8
00:00:22,140 --> 00:00:30,539
we can use bang cpuid or exclamation

9
00:00:27,000 --> 00:00:33,960
mark cpuid command for this purpose and

10
00:00:30,539 --> 00:00:36,920
it tries to monitor all of the CPU IDs

11
00:00:33,960 --> 00:00:41,899
that that are executed in their Windows

12
00:00:36,920 --> 00:00:45,860
I made some examples for it

13
00:00:41,899 --> 00:00:49,200
uh generally cpuid instruction is

14
00:00:45,860 --> 00:00:51,059
responsible for uh showing whether some

15
00:00:49,200 --> 00:00:53,960
of the features are supported in the

16
00:00:51,059 --> 00:00:53,960
processor or not

17
00:00:54,079 --> 00:01:04,320
or other signatures of the processor uh

18
00:00:59,360 --> 00:01:06,119
basically if we try to change them we

19
00:01:04,320 --> 00:01:09,000
could use HyperDbg for this

20
00:01:06,119 --> 00:01:11,939
purpose but

21
00:01:09,000 --> 00:01:15,360
let's see let's see some examples I made

22
00:01:11,939 --> 00:01:17,420
a cpuid test uh

23
00:01:15,360 --> 00:01:21,900
program

24
00:01:17,420 --> 00:01:23,900
that basically executes a

25
00:01:21,900 --> 00:01:27,720
special

26
00:01:23,900 --> 00:01:30,479
a special cpuid instruction with a

27
00:01:27,720 --> 00:01:33,840
special index the index of the cpuid is

28
00:01:30,479 --> 00:01:38,060
always in the EA ex register

29
00:01:33,840 --> 00:01:38,060
so let's try to run it

30
00:01:39,299 --> 00:01:47,220
yeah here's the result for this uh CPU

31
00:01:43,380 --> 00:01:52,380
ID instructions on my system uh

32
00:01:47,220 --> 00:01:58,439
if you search uh Google for uh some of

33
00:01:52,380 --> 00:01:58,439
the indexes of uh cpuid instruction

34
00:01:59,340 --> 00:02:04,740
we could see the details for these CPU

35
00:02:02,939 --> 00:02:09,599
ID indexes

36
00:02:04,740 --> 00:02:12,920
I made some examples here

37
00:02:09,599 --> 00:02:17,160
let's try to

38
00:02:12,920 --> 00:02:17,160
see them yeah

39
00:02:22,319 --> 00:02:26,120
now here is the only

40
00:02:26,520 --> 00:02:33,540
um the Wikipedia link also explains some

41
00:02:30,120 --> 00:02:38,459
of the cpuid for example with eax which

42
00:02:33,540 --> 00:02:42,840
is equals to zero we could expect these

43
00:02:38,459 --> 00:02:45,180
values while if the x is one then it uh

44
00:02:42,840 --> 00:02:46,800
reports some of the processor infline

45
00:02:45,180 --> 00:02:51,599
feature bits

46
00:02:46,800 --> 00:02:55,379
so this is uh this is how edicts and ecx

47
00:02:51,599 --> 00:02:59,599
Refugee stairs are formed after running

48
00:02:55,379 --> 00:03:01,739
cpuid with a one eax

49
00:02:59,599 --> 00:03:04,680
these are the features that are

50
00:03:01,739 --> 00:03:08,940
supported or that are reported here so

51
00:03:04,680 --> 00:03:15,560
now let's try to just test them

52
00:03:08,940 --> 00:03:15,560
uh you can find uh find this uh

53
00:03:16,620 --> 00:03:22,620
project in the

54
00:03:19,800 --> 00:03:29,220
files aligned with the

55
00:03:22,620 --> 00:03:30,659
tutorial uh let's try to run cpuid test

56
00:03:29,220 --> 00:03:32,700
here

57
00:03:30,659 --> 00:03:36,440
and

58
00:03:32,700 --> 00:03:36,440
just tries to

59
00:03:36,540 --> 00:03:41,900
click here I forgot to just

60
00:03:42,180 --> 00:03:46,440
change it to

61
00:03:52,500 --> 00:03:57,060
yeah

62
00:03:54,060 --> 00:03:57,060
so

63
00:03:58,260 --> 00:04:05,400
basically we will change it to

64
00:04:00,299 --> 00:04:07,620
multitraded debug here and Chase up

65
00:04:05,400 --> 00:04:11,879
this we will change it to a

66
00:04:07,620 --> 00:04:14,400
multi-trailer dll which is

67
00:04:11,879 --> 00:04:16,859
also should be checked amounts traded

68
00:04:14,400 --> 00:04:20,660
that's okay

69
00:04:16,859 --> 00:04:20,660
no I try to

70
00:04:23,699 --> 00:04:28,160
remove it

71
00:04:25,440 --> 00:04:28,160
and again

72
00:04:28,320 --> 00:04:30,320
um

73
00:04:33,300 --> 00:04:37,699
Windows tries to escape

74
00:04:40,500 --> 00:04:49,520
yeah here are the results uh for this uh

75
00:04:46,699 --> 00:04:54,419
VMware Workstation machine

76
00:04:49,520 --> 00:04:58,199
uh so in case if I want to use here it's

77
00:04:54,419 --> 00:05:01,440
zero then these uh values are reported

78
00:04:58,199 --> 00:05:05,759
and if I set eax to 1 then these values

79
00:05:01,440 --> 00:05:09,120
are reported so uh basically if we want

80
00:05:05,759 --> 00:05:12,860
to just modify one of them then we could

81
00:05:09,120 --> 00:05:17,400
use some of the scripts here for example

82
00:05:12,860 --> 00:05:23,759
in this script I try to simply change

83
00:05:17,400 --> 00:05:27,120
the cpuid 0 and set EB ecx and etx to

84
00:05:23,759 --> 00:05:31,680
zero and also try to short circuit uh

85
00:05:27,120 --> 00:05:35,539
the event so I I'm sure that uh the the

86
00:05:31,680 --> 00:05:39,840
system won't execute this cpuid only

87
00:05:35,539 --> 00:05:43,080
these values are reported to the uh

88
00:05:39,840 --> 00:05:45,720
application so now let's try to run this

89
00:05:43,080 --> 00:05:49,979
application as you can see here is the

90
00:05:45,720 --> 00:05:54,060
results but in case if I

91
00:05:49,979 --> 00:05:58,259
try to pass hybrid Imaging and run this

92
00:05:54,060 --> 00:06:00,900
a script then after that I try to run

93
00:05:58,259 --> 00:06:03,660
the cpuid test again and you can see

94
00:06:00,900 --> 00:06:09,479
that these values are changed to zero

95
00:06:03,660 --> 00:06:09,479
because uh we change them here

96
00:06:09,840 --> 00:06:14,900
and nothing happens like the the ex is

97
00:06:12,960 --> 00:06:18,180
also zero because

98
00:06:14,900 --> 00:06:20,820
uh the index was zero and the short

99
00:06:18,180 --> 00:06:22,340
circuit the event but in case if we

100
00:06:20,820 --> 00:06:26,000
remove

101
00:06:22,340 --> 00:06:30,840
this event again uh

102
00:06:26,000 --> 00:06:33,060
we see that the actual values are

103
00:06:30,840 --> 00:06:36,840
displayed here

104
00:06:33,060 --> 00:06:38,819
this was a really simple example uh

105
00:06:36,840 --> 00:06:40,020
there is a

106
00:06:38,819 --> 00:06:43,259
uh

107
00:06:40,020 --> 00:06:45,600
an application cpu-z which is

108
00:06:43,259 --> 00:06:47,639
responsible for reporting some of this

109
00:06:45,600 --> 00:06:51,479
processor features you can download them

110
00:06:47,639 --> 00:06:53,300
online there's a portable edition of

111
00:06:51,479 --> 00:06:59,780
this CQC

112
00:06:53,300 --> 00:06:59,780
uh program I try to just paste it here

113
00:07:01,620 --> 00:07:09,500
um again I try to run it

114
00:07:05,759 --> 00:07:09,500
and as you can see

115
00:07:12,120 --> 00:07:19,259
it detects my processor uh detects the

116
00:07:17,100 --> 00:07:21,800
features or the instructions that are

117
00:07:19,259 --> 00:07:25,080
supported by my processor

118
00:07:21,800 --> 00:07:28,380
along with some other informations and

119
00:07:25,080 --> 00:07:31,400
also detects that my machine is an Intel

120
00:07:28,380 --> 00:07:35,639
Core i5

121
00:07:31,400 --> 00:07:37,280
so let's just try we know that this CPU

122
00:07:35,639 --> 00:07:40,800
CPU Z

123
00:07:37,280 --> 00:07:43,139
program tries to use some cpuid

124
00:07:40,800 --> 00:07:47,400
instructions

125
00:07:43,139 --> 00:07:51,360
so let's try to modify them uh for the

126
00:07:47,400 --> 00:07:51,360
first example uh

127
00:07:52,080 --> 00:07:56,900
uh skip this example this is the same as

128
00:07:55,979 --> 00:08:02,880
the

129
00:07:56,900 --> 00:08:06,680
first one it tries to put a CPU id1 in

130
00:08:02,880 --> 00:08:09,440
cpuid with eax index one to

131
00:08:06,680 --> 00:08:13,080
these uh

132
00:08:09,440 --> 00:08:16,080
values we already test the same command

133
00:08:13,080 --> 00:08:17,660
here no need to test it anymore but

134
00:08:16,080 --> 00:08:21,259
let's

135
00:08:17,660 --> 00:08:27,440
let's a little bit change the

136
00:08:21,259 --> 00:08:27,440
uh cpuide is zero to some other uh

137
00:08:29,099 --> 00:08:34,640
like for example if you see from the uh

138
00:08:33,479 --> 00:08:40,919
first

139
00:08:34,640 --> 00:08:46,560
from the zeroth uh EA x uh this tries in

140
00:08:40,919 --> 00:08:50,540
this in this uh cpuid tries to show

141
00:08:46,560 --> 00:08:50,540
some some of the uh

142
00:08:50,580 --> 00:08:56,580
the signature of the processor for

143
00:08:52,860 --> 00:08:59,279
example uh if for for example uh for

144
00:08:56,580 --> 00:09:01,459
Intel processor this signature is shown

145
00:08:59,279 --> 00:09:04,620
or for example for AMD processor

146
00:09:01,459 --> 00:09:09,180
authentic AMD or for Intel genuine

147
00:09:04,620 --> 00:09:13,019
genuine Intel is showed there are also

148
00:09:09,180 --> 00:09:15,060
different uh very different uh names for

149
00:09:13,019 --> 00:09:19,440
different processors

150
00:09:15,060 --> 00:09:22,019
uh uh so I try to just change this uh

151
00:09:19,440 --> 00:09:28,260
this Intel signature which we accept

152
00:09:22,019 --> 00:09:30,480
which we expect to see uh this

153
00:09:28,260 --> 00:09:34,740
signature for Intel parser I try to

154
00:09:30,480 --> 00:09:40,940
change it to an authentic AMD so

155
00:09:34,740 --> 00:09:44,720
basically if I try to uh convert uh

156
00:09:40,940 --> 00:09:44,720
ASCII uh

157
00:09:46,019 --> 00:09:51,660
um the hexag small to ASCII

158
00:09:53,640 --> 00:10:00,600
you can see that I changed the signature

159
00:09:58,740 --> 00:10:03,440
of the processor

160
00:10:00,600 --> 00:10:03,440
to

161
00:10:04,560 --> 00:10:08,120
authentic uh

162
00:10:11,820 --> 00:10:14,779
AMD

163
00:10:20,100 --> 00:10:27,660
it's a little bit awkward how it shows

164
00:10:23,640 --> 00:10:29,660
it here because you have to change it

165
00:10:27,660 --> 00:10:29,660
um

166
00:10:31,320 --> 00:10:39,959
in three four bit registers so basically

167
00:10:34,800 --> 00:10:42,959
it's something like this uh authentic

168
00:10:39,959 --> 00:10:42,959
AMD

169
00:10:44,600 --> 00:10:48,600
at 10.

170
00:10:51,480 --> 00:10:57,620
a u t h

171
00:10:54,620 --> 00:10:59,640
e n t i

172
00:10:57,620 --> 00:11:01,260
uh c-a-m-d

173
00:10:59,640 --> 00:11:03,720
[pause]

174
00:11:01,260 --> 00:11:05,399
from right to left you have to read it

175
00:11:03,720 --> 00:11:07,680
because we want to put them in a

176
00:11:05,399 --> 00:11:11,040
register

177
00:11:07,680 --> 00:11:15,180
uh let's let's try let's try to run uh

178
00:11:11,040 --> 00:11:19,680
the same command uh on my machine and I

179
00:11:15,180 --> 00:11:22,820
also uh ignored by short circuiting the

180
00:11:19,680 --> 00:11:22,820
events so

181
00:11:26,040 --> 00:11:33,140
here I try to run

182
00:11:29,399 --> 00:11:33,140
this cpuid again

183
00:11:40,680 --> 00:11:45,200
and as you can see it it can no longer

184
00:11:43,740 --> 00:11:49,380
uh

185
00:11:45,200 --> 00:11:52,320
recognize my processor so it just tries

186
00:11:49,380 --> 00:11:56,880
it just thinks that we're running on an

187
00:11:52,320 --> 00:11:58,500
AMD processor uh I run it previously and

188
00:11:56,880 --> 00:12:01,800
this is the new window

189
00:11:58,500 --> 00:12:03,959
so yeah this is how we change the

190
00:12:01,800 --> 00:12:06,800
signature of the processor and Hyper

191
00:12:03,959 --> 00:12:09,779
division by using a simple script

192
00:12:06,800 --> 00:12:12,120
and we can fool some of the programs for

193
00:12:09,779 --> 00:12:15,300
example to think that the current

194
00:12:12,120 --> 00:12:19,019
processor is different we could do that

195
00:12:15,300 --> 00:12:21,540
uh let's try to just remove

196
00:12:19,019 --> 00:12:25,459
the effects of this uh

197
00:12:21,540 --> 00:12:29,760
the script and run another script

198
00:12:25,459 --> 00:12:32,839
uh if as you can see from the correct

199
00:12:29,760 --> 00:12:35,300
version of the uh

200
00:12:32,839 --> 00:12:38,760
cpu-z uh

201
00:12:35,300 --> 00:12:45,480
we support these instruction sets for

202
00:12:38,760 --> 00:12:48,800
example VT-x AES ABX ABS 2 sse2 SSE and

203
00:12:45,480 --> 00:12:52,920
sse3 are also supported by my processor

204
00:12:48,800 --> 00:12:56,760
but in case if you want to uh like mask

205
00:12:52,920 --> 00:13:00,180
some of them in the cpuid you can all

206
00:12:56,760 --> 00:13:04,260
you can you could again use them if I

207
00:13:00,180 --> 00:13:11,519
return to the Wikipedia page here

208
00:13:04,260 --> 00:13:11,519
you can see that for cpuid with eax1

209
00:13:11,579 --> 00:13:16,579
uh uh in the ecx register

210
00:13:16,680 --> 00:13:19,940
let's search for

211
00:13:20,339 --> 00:13:23,300
ethics

212
00:13:24,680 --> 00:13:34,519
yeah in ecx register uh when cpuid is is

213
00:13:31,139 --> 00:13:34,519
one and then

214
00:13:34,740 --> 00:13:42,079
the fifth uh

215
00:13:37,740 --> 00:13:44,940
bits of ecx shows the

216
00:13:42,079 --> 00:13:50,880
shows whether the processor supports

217
00:13:44,940 --> 00:13:55,200
mimics or not so I try to simply uh I

218
00:13:50,880 --> 00:13:58,200
knew that cpuid1 executes

219
00:13:55,200 --> 00:14:02,899
by

220
00:13:58,200 --> 00:14:02,899
these values let's try to see it

221
00:14:05,040 --> 00:14:11,820
CPU id1 give gives me these values in

222
00:14:09,600 --> 00:14:15,959
this uh

223
00:14:11,820 --> 00:14:19,079
virtual machine so I try to set all of

224
00:14:15,959 --> 00:14:22,980
them uh like a normal machine and

225
00:14:19,079 --> 00:14:24,680
instead of that I try to mask the fifth

226
00:14:22,980 --> 00:14:28,380
bits of

227
00:14:24,680 --> 00:14:31,200
the ecx register so let's try to run it

228
00:14:28,380 --> 00:14:33,860
again on the machine to see what happens

229
00:14:31,200 --> 00:14:33,860
here

230
00:14:35,839 --> 00:14:44,839
I try to run this CPU Z

231
00:14:42,060 --> 00:14:44,839
again

232
00:14:52,500 --> 00:14:58,880
and and yeah and it also won't update

233
00:14:55,500 --> 00:15:02,579
the only first time the cpuid is

234
00:14:58,880 --> 00:15:05,820
executed and it continues to show it uh

235
00:15:02,579 --> 00:15:09,240
as you can see from uh the right panel

236
00:15:05,820 --> 00:15:13,680
uh there is no like no longer we see uh

237
00:15:09,240 --> 00:15:18,959
VMX or VT-x feature we have VMX here in

238
00:15:13,680 --> 00:15:18,959
the correct version of the uh

239
00:15:19,519 --> 00:15:27,060
uh cpuid but in this case we don't have

240
00:15:23,279 --> 00:15:30,000
VMX anymore but keep in mind that uh by

241
00:15:27,060 --> 00:15:33,260
using this command you you can just fool

242
00:15:30,000 --> 00:15:36,120
some of the applications like CPU cpu-z

243
00:15:33,260 --> 00:15:38,579
uh that this feature is not supported

244
00:15:36,120 --> 00:15:41,100
but still this feature is supported in

245
00:15:38,579 --> 00:15:43,500
case if you want to disable the support

246
00:15:41,100 --> 00:15:45,660
for a special feature you should use for

247
00:15:43,500 --> 00:15:48,720
example the msri

248
00:15:45,660 --> 00:15:50,399
or other commands that are related to

249
00:15:48,720 --> 00:15:54,839
the control registers

250
00:15:50,399 --> 00:15:56,940
for example by using MSR right you can

251
00:15:54,839 --> 00:16:00,180
disable some of some of these features

252
00:15:56,940 --> 00:16:02,579
if the processor or if an application or

253
00:16:00,180 --> 00:16:04,980
a driver or the operating system tries

254
00:16:02,579 --> 00:16:08,579
to activate some of the features you can

255
00:16:04,980 --> 00:16:12,360
uh actually perform actually disable

256
00:16:08,579 --> 00:16:15,779
them by using MSR right event command

257
00:16:12,360 --> 00:16:18,300
but the cpuid is just a notification

258
00:16:15,779 --> 00:16:20,100
it's just that like the processor wants

259
00:16:18,300 --> 00:16:23,459
to say something that whether it's

260
00:16:20,100 --> 00:16:29,040
supported or not it's up to uh the

261
00:16:23,459 --> 00:16:31,680
program to configure it to just like see

262
00:16:29,040 --> 00:16:34,019
that it's not supported and uh won't

263
00:16:31,680 --> 00:16:35,760
perform other checks it's up to you you

264
00:16:34,019 --> 00:16:38,660
can you can decide whether to disable it

265
00:16:35,760 --> 00:16:42,779
by using MSN register or just disabling

266
00:16:38,660 --> 00:16:45,540
uh for the application to just give give

267
00:16:42,779 --> 00:16:49,880
the process give the program some ideas

268
00:16:45,540 --> 00:16:49,880
whether the feature is supported or not

