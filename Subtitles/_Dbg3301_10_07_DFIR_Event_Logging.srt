1
00:00:00,060 --> 00:00:05,899
now let's see how we can use HyperDbg

2
00:00:03,480 --> 00:00:09,900
for digital forensics and incidence

3
00:00:05,899 --> 00:00:13,320
response a lot of hyper DVD features

4
00:00:09,900 --> 00:00:15,240
might be used for this purpose here

5
00:00:13,320 --> 00:00:18,480
hybrid which provides an event

6
00:00:15,240 --> 00:00:21,420
forwarding mechanism which is designed

7
00:00:18,480 --> 00:00:24,320
to make a hybrid u g a tool for

8
00:00:21,420 --> 00:00:27,480
gathering gathering different logs and

9
00:00:24,320 --> 00:00:30,720
analyzing the system behavior

10
00:00:27,480 --> 00:00:33,840
or even forwarding currently supports

11
00:00:30,720 --> 00:00:37,140
external sources like files name pipe

12
00:00:33,840 --> 00:00:39,840
and TCP circuits and a new command

13
00:00:37,140 --> 00:00:42,300
called output is added to the hybrid

14
00:00:39,840 --> 00:00:45,780
widget to create or access the remote

15
00:00:42,300 --> 00:00:48,180
resources so if we want to use the event

16
00:00:45,780 --> 00:00:51,180
forwarding we should all we we have to

17
00:00:48,180 --> 00:00:54,719
provide several steps like first create

18
00:00:51,180 --> 00:00:57,180
a resource then for a second we should

19
00:00:54,719 --> 00:01:00,600
open the remote resource and then we can

20
00:00:57,180 --> 00:01:03,420
use this resource in all of the events

21
00:01:00,600 --> 00:01:07,020
that last we should also consider

22
00:01:03,420 --> 00:01:10,380
closing the resource now let's

23
00:01:07,020 --> 00:01:12,479
see how we can use this command let's

24
00:01:10,380 --> 00:01:13,979
just perform this slide and then then we

25
00:01:12,479 --> 00:01:16,979
will see an example

26
00:01:13,979 --> 00:01:21,119
first we will use output create and then

27
00:01:16,979 --> 00:01:24,119
we will specify a name for the inputs

28
00:01:21,119 --> 00:01:27,299
and output resource in this example I

29
00:01:24,119 --> 00:01:30,799
choose my output name and then we

30
00:01:27,299 --> 00:01:34,380
specify the type of

31
00:01:30,799 --> 00:01:38,340
resource in this example it's a file so

32
00:01:34,380 --> 00:01:41,520
if as is as it's a file then we will

33
00:01:38,340 --> 00:01:43,979
specify the file name if it's a TCP

34
00:01:41,520 --> 00:01:47,220
connection or a TCP a circuit connection

35
00:01:43,979 --> 00:01:49,200
then after that we will specify the port

36
00:01:47,220 --> 00:01:51,899
address and the port

37
00:01:49,200 --> 00:01:54,540
those other resources available for

38
00:01:51,899 --> 00:01:58,020
example name pipe are also supported by

39
00:01:54,540 --> 00:02:01,920
hyper energy you could also pass the

40
00:01:58,020 --> 00:02:05,340
results to to an into a name pipe

41
00:02:01,920 --> 00:02:08,160
after that we have to open the resource

42
00:02:05,340 --> 00:02:11,819
uh we could not access to a closed

43
00:02:08,160 --> 00:02:14,520
resource and then we could use different

44
00:02:11,819 --> 00:02:17,940
events in HyperDbg we could add an

45
00:02:14,520 --> 00:02:20,340
output argument to the events and

46
00:02:17,940 --> 00:02:24,140
specify the name of the resource that we

47
00:02:20,340 --> 00:02:27,780
previously created we could also use

48
00:02:24,140 --> 00:02:30,480
several sources or output sources we

49
00:02:27,780 --> 00:02:32,400
could pass the result to the several

50
00:02:30,480 --> 00:02:35,400
courses for example we could pass it to

51
00:02:32,400 --> 00:02:39,060
a file while we are also passing it to a

52
00:02:35,400 --> 00:02:42,060
TCP connection or a name pipe resource

53
00:02:39,060 --> 00:02:44,400
and when we finished we should close it

54
00:02:42,060 --> 00:02:47,700
and you should also consider that you

55
00:02:44,400 --> 00:02:52,140
cannot reopen the resource once it's

56
00:02:47,700 --> 00:02:56,060
closed so let's see how we could use

57
00:02:52,140 --> 00:02:56,060
this feature in hybrid imagery

58
00:02:56,459 --> 00:03:03,019
to try to run HyperDbg in VMI mode here

59
00:03:16,620 --> 00:03:25,500
there's a command called output this is

60
00:03:21,180 --> 00:03:30,480
example we could use output to create my

61
00:03:25,500 --> 00:03:34,200
output name again with this file name

62
00:03:30,480 --> 00:03:37,739
I want to just simply move the results

63
00:03:34,200 --> 00:03:40,680
of an event through a simple file

64
00:03:37,739 --> 00:03:43,080
after that we could query about the

65
00:03:40,680 --> 00:03:46,560
resource as we can see here the the

66
00:03:43,080 --> 00:03:50,580
output is the now not open so we could

67
00:03:46,560 --> 00:03:53,940
use output and opening

68
00:03:50,580 --> 00:03:56,519
then we should specify its name now if

69
00:03:53,940 --> 00:03:58,560
we query it again we will see that their

70
00:03:56,519 --> 00:04:02,700
resources open

71
00:03:58,560 --> 00:04:05,340
after that we could use this resource in

72
00:04:02,700 --> 00:04:09,120
different commands for example I will or

73
00:04:05,340 --> 00:04:12,840
I'll write a simple script which prints

74
00:04:09,120 --> 00:04:15,799
the system call uh

75
00:04:12,840 --> 00:04:15,799
number

76
00:04:24,000 --> 00:04:29,000
and

77
00:04:25,020 --> 00:04:33,060
we will uh we will all we will pass this

78
00:04:29,000 --> 00:04:37,500
resource to the uh

79
00:04:33,060 --> 00:04:39,020
HyperDbg let me see it's uh syntax is

80
00:04:37,500 --> 00:04:46,160
called

81
00:04:39,020 --> 00:04:46,160
out put it again to this resource

82
00:04:47,160 --> 00:04:54,479
and now whatever this is called

83
00:04:51,660 --> 00:04:58,440
gathers from the system is saved into

84
00:04:54,479 --> 00:05:02,520
this file I I could just

85
00:04:58,440 --> 00:05:06,300
close uh clear the events here

86
00:05:02,520 --> 00:05:10,380
you will see that it's no longer safe in

87
00:05:06,300 --> 00:05:14,120
the uh output file but if we just try to

88
00:05:10,380 --> 00:05:14,120
close uh the output

89
00:05:16,759 --> 00:05:21,380
let's just see its name

90
00:05:27,240 --> 00:05:35,660
there's no event here I will try to

91
00:05:31,380 --> 00:05:35,660
close this output

92
00:05:39,840 --> 00:05:47,160
event why I use even

93
00:05:43,259 --> 00:05:49,039
uh it's called output yeah it's now

94
00:05:47,160 --> 00:05:51,900
closed

95
00:05:49,039 --> 00:05:54,300
let's see it's the states again now it

96
00:05:51,900 --> 00:05:56,639
indicates that it's closed and if we see

97
00:05:54,300 --> 00:06:01,440
the file that we're gonna we can see

98
00:05:56,639 --> 00:06:03,539
that the system calls uh are saved into

99
00:06:01,440 --> 00:06:06,240
this file

100
00:06:03,539 --> 00:06:09,120
and this is a pretty useful feature that

101
00:06:06,240 --> 00:06:11,699
you can use in order to automate Patcher

102
00:06:09,120 --> 00:06:14,160
scripts and use these resources to

103
00:06:11,699 --> 00:06:17,120
gather different information from the

104
00:06:14,160 --> 00:06:17,120
from your system

