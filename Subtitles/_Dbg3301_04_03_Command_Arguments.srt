1
00:00:00,140 --> 00:00:07,040
let's see how we can pass arguments to

2
00:00:04,080 --> 00:00:10,639
the script engine of the HyperDbg

3
00:00:07,040 --> 00:00:13,380
arguments or just like pseudo registers

4
00:00:10,639 --> 00:00:17,279
interpreted as to the register so you

5
00:00:13,380 --> 00:00:21,539
can pass them like dollar sign Arc zero

6
00:00:17,279 --> 00:00:23,760
or dollar sign or one uh and so on and

7
00:00:21,539 --> 00:00:26,160
this is everything is just going to

8
00:00:23,760 --> 00:00:29,359
change the you can just put our

9
00:00:26,160 --> 00:00:32,700
arguments uh you can put X has

10
00:00:29,359 --> 00:00:37,500
expressions as arguments the thing is

11
00:00:32,700 --> 00:00:40,140
that hyper WG uses a DOT DS or dark

12
00:00:37,500 --> 00:00:43,920
debugger script extension for force

13
00:00:40,140 --> 00:00:46,980
script files and just like the C uh

14
00:00:43,920 --> 00:00:50,760
programming CRC++ that or the

15
00:00:46,980 --> 00:00:55,500
simple main program of regular programs

16
00:00:50,760 --> 00:01:00,199
are 0 pointed to the pass or the exit

17
00:00:55,500 --> 00:01:03,600
the location of the .ds by other

18
00:01:00,199 --> 00:01:06,560
arguments or might be an expression

19
00:01:03,600 --> 00:01:09,900
might be a constant or might be an

20
00:01:06,560 --> 00:01:12,119
extreme but everything you should keep

21
00:01:09,900 --> 00:01:15,240
in mind that everything is interpreted

22
00:01:12,119 --> 00:01:17,880
in the hexadecimal format if you don't

23
00:01:15,240 --> 00:01:21,180
specific any prefixes but for example

24
00:01:17,880 --> 00:01:24,780
you can change it to a decimal format if

25
00:01:21,180 --> 00:01:27,479
you specify zero and prefix before your

26
00:01:24,780 --> 00:01:30,720
number let's see another example for

27
00:01:27,479 --> 00:01:33,960
example uh this is my script if I want

28
00:01:30,720 --> 00:01:36,780
to run this script it's simply between

29
00:01:33,960 --> 00:01:38,460
two query brackets so it's a

30
00:01:36,780 --> 00:01:42,360
multi-linear script and the first

31
00:01:38,460 --> 00:01:46,259
argument is passed as dollar sign Arc

32
00:01:42,360 --> 00:01:50,640
one Arc one and the second one is past

33
00:01:46,259 --> 00:01:54,479
asked darshan Arc if you want to run the

34
00:01:50,640 --> 00:01:57,240
above a scripts we have two options here

35
00:01:54,479 --> 00:02:00,060
the first option is that we can use the

36
00:01:57,240 --> 00:02:05,159
data script command in HyperDbg and

37
00:02:00,060 --> 00:02:08,159
simply add a pass to the SQL and use the

38
00:02:05,159 --> 00:02:08,159
uh

