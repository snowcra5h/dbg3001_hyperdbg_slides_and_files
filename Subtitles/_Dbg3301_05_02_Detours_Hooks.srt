1
00:00:00,000 --> 00:00:05,759
but there's also another type of books

2
00:00:03,360 --> 00:00:07,400
which are detours the style books which

3
00:00:05,759 --> 00:00:11,219
are simply

4
00:00:07,400 --> 00:00:14,099
books EPT hooks that are not working

5
00:00:11,219 --> 00:00:17,640
with a breakpoint or a freedom

6
00:00:14,099 --> 00:00:21,840
breakpoint and uh these hooks are simple

7
00:00:17,640 --> 00:00:27,720
detours uh books the thing is that uh

8
00:00:21,840 --> 00:00:29,939
these hooks just replace uh the start of

9
00:00:27,720 --> 00:00:33,180
a function or the routine that you want

10
00:00:29,939 --> 00:00:36,960
with some target functions or a trap

11
00:00:33,180 --> 00:00:39,480
line who just move the execution flow to

12
00:00:36,960 --> 00:00:42,899
another function or to a HyperDbg

13
00:00:39,480 --> 00:00:45,360
function and after that it will try to

14
00:00:42,899 --> 00:00:47,520
execute those instructions that are

15
00:00:45,360 --> 00:00:49,440
patched and that are not available in

16
00:00:47,520 --> 00:00:51,960
the target function and inflow

17
00:00:49,440 --> 00:00:56,760
eventually return to the normal

18
00:00:51,960 --> 00:01:02,039
execution flow it's faster than uh hyper

19
00:00:56,760 --> 00:01:06,180
images EPT hook we use EPT hook 2

20
00:01:02,039 --> 00:01:08,880
command for these detours hook but it's

21
00:01:06,180 --> 00:01:11,700
definitely faster than EPT hooks but it

22
00:01:08,880 --> 00:01:16,080
has some limitations for example the the

23
00:01:11,700 --> 00:01:19,979
thing about this instruction about this

24
00:01:16,080 --> 00:01:23,460
command is that it operates on kernel

25
00:01:19,979 --> 00:01:26,580
mode rather than being extrude mode most

26
00:01:23,460 --> 00:01:29,280
of the script engine functions are

27
00:01:26,580 --> 00:01:32,700
designed to run on Venus known root mode

28
00:01:29,280 --> 00:01:36,900
a vmix root mode but this function runs

29
00:01:32,700 --> 00:01:40,439
on remix non-root mode so it won't cause

30
00:01:36,900 --> 00:01:43,439
a remix it some of the functions is of

31
00:01:40,439 --> 00:01:46,979
the script engine won't work for this

32
00:01:43,439 --> 00:01:49,320
command and the thing that makes it

33
00:01:46,979 --> 00:01:52,020
faster than npt hood is that it won't

34
00:01:49,320 --> 00:01:54,240
cause a VM exit so everything is handled

35
00:01:52,020 --> 00:01:57,720
in the kernel level and there's no VMX

36
00:01:54,240 --> 00:02:01,140
it but it generally preferred to use the

37
00:01:57,720 --> 00:02:03,840
APT hook I mean it's better to in most

38
00:02:01,140 --> 00:02:07,020
of the scenarios it's better to use the

39
00:02:03,840 --> 00:02:10,080
APT hook but in some cases when you want

40
00:02:07,020 --> 00:02:14,040
to check a function faster you can use

41
00:02:10,080 --> 00:02:16,020
apt hook to and another thing is that it

42
00:02:14,040 --> 00:02:19,140
can only be used in the kernel address

43
00:02:16,020 --> 00:02:21,959
since the APT hook itself can be applied

44
00:02:19,140 --> 00:02:24,239
in on both the user mode addresses and

45
00:02:21,959 --> 00:02:28,379
kernel mode addresses but this function

46
00:02:24,239 --> 00:02:30,000
apt hoop 2 is only limited to the kernel

47
00:02:28,379 --> 00:02:32,400
mode addresses which means that you

48
00:02:30,000 --> 00:02:36,840
cannot use it for the user mode

49
00:02:32,400 --> 00:02:39,360
addresses and also you can only use one

50
00:02:36,840 --> 00:02:42,060
hook in a page of memory for example if

51
00:02:39,360 --> 00:02:45,900
you put a hook on this address you

52
00:02:42,060 --> 00:02:49,379
cannot put another book on the boundary

53
00:02:45,900 --> 00:02:51,780
of that page for example we can not hook

54
00:02:49,379 --> 00:02:53,540
starting from this address to this

55
00:02:51,780 --> 00:02:56,819
address because

56
00:02:53,540 --> 00:02:59,459
these addresses are located on the same

57
00:02:56,819 --> 00:03:01,739
page another thing is about the

58
00:02:59,459 --> 00:03:06,180
limitation enough detours style Hook is

59
00:03:01,739 --> 00:03:09,540
that it patches 19 bikes from from the

60
00:03:06,180 --> 00:03:12,120
memory so you have to make sure that

61
00:03:09,540 --> 00:03:14,700
there is no relative jump or a relative

62
00:03:12,120 --> 00:03:17,220
car it has the classic problem of

63
00:03:14,700 --> 00:03:20,580
detours who will have to make sure that

64
00:03:17,220 --> 00:03:23,459
there is no relative instruction within

65
00:03:20,580 --> 00:03:27,060
the boundary that you want to put a hook

66
00:03:23,459 --> 00:03:29,879
on it and the good thing is that most of

67
00:03:27,060 --> 00:03:32,400
the time in x64 fast calculator

68
00:03:29,879 --> 00:03:36,060
convention functions and most of the

69
00:03:32,400 --> 00:03:39,300
time the first 19 bytes that the

70
00:03:36,060 --> 00:03:42,599
functions are not starting with a really

71
00:03:39,300 --> 00:03:45,659
a relative job or relative cut this

72
00:03:42,599 --> 00:03:48,840
command operates in remix non-root mode

73
00:03:45,659 --> 00:03:51,120
which means that because hybrid which is

74
00:03:48,840 --> 00:03:52,980
a script engine as I described it before

75
00:03:51,120 --> 00:03:56,040
it's mainly designed to work on remix

76
00:03:52,980 --> 00:03:57,959
root mode so you have limitations on the

77
00:03:56,040 --> 00:04:01,140
script engine which is uh truly

78
00:03:57,959 --> 00:04:05,940
described here let me show it to you for

79
00:04:01,140 --> 00:04:08,400
example you cannot access rip or rsp or

80
00:04:05,940 --> 00:04:11,400
our flags instructions because they use

81
00:04:08,400 --> 00:04:13,799
VMware and VM write instructions in the

82
00:04:11,400 --> 00:04:15,500
script engine of the hybrid images so

83
00:04:13,799 --> 00:04:18,299
there are some limitations and

84
00:04:15,500 --> 00:04:22,019
considerations for using the script

85
00:04:18,299 --> 00:04:25,979
engine In apk 2 but it's completely okay

86
00:04:22,019 --> 00:04:29,340
to use everything in apt hook the

87
00:04:25,979 --> 00:04:33,740
regular if it hook and is also prefer to

88
00:04:29,340 --> 00:04:33,740
use apt hook over if you took two

