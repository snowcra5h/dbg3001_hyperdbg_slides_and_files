1
00:00:00,000 --> 00:00:05,400
for now I just basically create a green

2
00:00:03,659 --> 00:00:09,900
dbg

3
00:00:05,400 --> 00:00:15,360
just to debug my uh VMware machine I

4
00:00:09,900 --> 00:00:17,820
just want to simply disable the driver's

5
00:00:15,360 --> 00:00:23,520
signature enforcement for the debugger

6
00:00:17,820 --> 00:00:23,520
and then I go to the hyper WG cell

7
00:00:26,640 --> 00:00:33,660
so let's just uh

8
00:00:30,240 --> 00:00:36,559
connect these

9
00:00:33,660 --> 00:00:40,260
debugger and debug it together

10
00:00:36,559 --> 00:00:43,260
you see how it works in the previous

11
00:00:40,260 --> 00:00:43,260
cards

12
00:00:46,739 --> 00:00:52,200
and here again I I will use my com port

13
00:00:50,760 --> 00:00:55,219
to connect

14
00:00:52,200 --> 00:00:57,780
debugger on the device

15
00:00:55,219 --> 00:01:00,920
as it's the first time that I'm running

16
00:00:57,780 --> 00:01:00,920
it it's available

17
00:01:02,059 --> 00:01:07,979
now you can see that the hybrid Refugee

18
00:01:05,400 --> 00:01:10,700
starts synchronizing simple details

19
00:01:07,979 --> 00:01:10,700
between the device

20
00:01:12,420 --> 00:01:19,380
um it's good to go

21
00:01:14,540 --> 00:01:21,000
I I will press the control plus C to

22
00:01:19,380 --> 00:01:23,040
just uh

23
00:01:21,000 --> 00:01:25,500
Alt

24
00:01:23,040 --> 00:01:29,700
the you're working as you can see that

25
00:01:25,500 --> 00:01:34,320
everything is halted here so I will use

26
00:01:29,700 --> 00:01:36,720
uh use it here let me just try to

27
00:01:34,320 --> 00:01:39,140
minimize it as long as

28
00:01:36,720 --> 00:01:39,140
so

29
00:01:40,460 --> 00:01:47,220
we can use the question mark command to

30
00:01:44,759 --> 00:01:50,939
evaluate everything for example if I

31
00:01:47,220 --> 00:01:52,320
want to see the registers

32
00:01:50,939 --> 00:01:56,100
the

33
00:01:52,320 --> 00:01:58,579
we almost just try to maximize itself

34
00:01:56,100 --> 00:01:58,579
why

35
00:02:00,060 --> 00:02:03,840
go

36
00:02:01,799 --> 00:02:06,220
here yeah

37
00:02:03,840 --> 00:02:08,640
it's clear

38
00:02:06,220 --> 00:02:12,060
[pause]

39
00:02:08,640 --> 00:02:14,700
you need to see there are some uh

40
00:02:12,060 --> 00:02:17,099
registers for example I want to see our

41
00:02:14,700 --> 00:02:22,020
X3 just there but that's not I want to

42
00:02:17,099 --> 00:02:23,360
do currently and let's see this example

43
00:02:22,020 --> 00:02:26,819
yeah

44
00:02:23,360 --> 00:02:30,300
uh here we have uh some some registers

45
00:02:26,819 --> 00:02:33,360
here uh I want to see whether these

46
00:02:30,300 --> 00:02:35,340
registers are valid or not uh for

47
00:02:33,360 --> 00:02:37,739
example I want to see whether this is a

48
00:02:35,340 --> 00:02:40,860
valid address or not I will simply use

49
00:02:37,739 --> 00:02:43,739
the DC command here and yeah this is a

50
00:02:40,860 --> 00:02:46,680
valid address but how can how can we

51
00:02:43,739 --> 00:02:49,860
check it by using a hybrid which is a

52
00:02:46,680 --> 00:02:52,940
script engine I just simply put a a

53
00:02:49,860 --> 00:02:56,099
question mark here and put it between

54
00:02:52,940 --> 00:02:58,560
these curly brackets and in the ql

55
00:02:56,099 --> 00:03:00,200
brackets I'll just simply put an if

56
00:02:58,560 --> 00:03:03,180
statement

57
00:03:00,200 --> 00:03:04,879
alongside me check

58
00:03:03,180 --> 00:03:11,300
address

59
00:03:04,879 --> 00:03:11,300
in the parameter I will pass I use

60
00:03:12,840 --> 00:03:19,519
which if it's equal to zero then

61
00:03:16,860 --> 00:03:19,519
friends

62
00:03:19,920 --> 00:03:22,519
f

63
00:03:23,940 --> 00:03:28,560
address is

64
00:03:26,879 --> 00:03:31,220
ready

65
00:03:28,560 --> 00:03:31,220
or

66
00:03:33,120 --> 00:03:39,120
if I just want to

67
00:03:36,019 --> 00:03:42,920
maybe there's an else's statement then

68
00:03:39,120 --> 00:03:42,920
I'll say that there

69
00:03:43,440 --> 00:03:48,180
address

70
00:03:45,480 --> 00:03:50,780
is not

71
00:03:48,180 --> 00:03:50,780
but

72
00:03:54,239 --> 00:04:01,680
um okay let's just close everything

73
00:03:58,220 --> 00:04:04,080
after that we can see that the our

74
00:04:01,680 --> 00:04:07,080
script is running there and the result

75
00:04:04,080 --> 00:04:11,640
is that others is ready but but just

76
00:04:07,080 --> 00:04:13,500
that let's just use the notepad here I

77
00:04:11,640 --> 00:04:17,060
just don't want to

78
00:04:13,500 --> 00:04:17,060
type it again and again

79
00:04:43,919 --> 00:04:46,580
okay

80
00:04:47,100 --> 00:04:49,580
[pause]

81
00:04:47,580 --> 00:04:49,580
um

82
00:04:49,919 --> 00:04:58,400
here we go this is our first example

83
00:04:53,880 --> 00:04:58,400
that we use HyperDbg

84
00:04:58,580 --> 00:05:03,780
we can also check for other addresses to

85
00:05:01,500 --> 00:05:06,900
for example we are pretty sure that

86
00:05:03,780 --> 00:05:09,419
currently rdx is not a valid address so

87
00:05:06,900 --> 00:05:12,440
let's check it here

88
00:05:09,419 --> 00:05:18,840
are excellent rdx

89
00:05:12,440 --> 00:05:20,520
if you use this uh simple script here oh

90
00:05:18,840 --> 00:05:22,100
let's copy it

91
00:05:20,520 --> 00:05:25,860
yeah

92
00:05:22,100 --> 00:05:28,139
I will use that you can it will see that

93
00:05:25,860 --> 00:05:29,660
there are our address is not valid if

94
00:05:28,139 --> 00:05:32,820
I'm gonna

95
00:05:29,660 --> 00:05:35,460
see the value exactly of the RDS

96
00:05:32,820 --> 00:05:37,400
registers uh it's pretty clear this is

97
00:05:35,460 --> 00:05:41,400
zero

98
00:05:37,400 --> 00:05:44,000
so it's not valid here

99
00:05:41,400 --> 00:05:44,000
another

100
00:05:44,160 --> 00:05:49,380
thing is that we're gonna write

101
00:05:47,460 --> 00:05:51,600
simple

102
00:05:49,380 --> 00:05:56,479
loop

103
00:05:51,600 --> 00:05:56,479
let's say it's not for loop which

104
00:05:59,400 --> 00:06:04,500
and

105
00:06:01,680 --> 00:06:05,820
the body after four you'll probably will

106
00:06:04,500 --> 00:06:08,840
stay there

107
00:06:05,820 --> 00:06:08,840
being death

108
00:06:12,259 --> 00:06:20,300
is the same as uh the C language is

109
00:06:15,960 --> 00:06:24,000
there except that there are a limited uh

110
00:06:20,300 --> 00:06:26,400
identifiers or uh there are a limited uh

111
00:06:24,000 --> 00:06:28,680
format students are supported by hybrids

112
00:06:26,400 --> 00:06:31,259
which is so make sure to check the

113
00:06:28,680 --> 00:06:34,319
document I will not show it this

114
00:06:31,259 --> 00:06:38,580
currently supported uh format a string

115
00:06:34,319 --> 00:06:41,160
uh identifier spot if you want to uh

116
00:06:38,580 --> 00:06:43,380
have an idea you can

117
00:06:41,160 --> 00:06:45,840
see the documentation and see whether

118
00:06:43,380 --> 00:06:48,660
it's supported or not because we are

119
00:06:45,840 --> 00:06:53,479
kind of working to add more identifiers

120
00:06:48,660 --> 00:06:53,479
and more support to more format history

121
00:06:53,780 --> 00:06:58,280
I will put I here

122
00:07:06,139 --> 00:07:10,639
I use the exact uh

123
00:07:13,500 --> 00:07:16,940
examples or use

124
00:07:18,800 --> 00:07:23,660
okay let's see where it works or not

125
00:07:25,139 --> 00:07:27,740
nope

126
00:07:32,460 --> 00:07:40,080
yeah you can see that it counts to 16

127
00:07:35,580 --> 00:07:43,500
because it's interpreted as a heads that

128
00:07:40,080 --> 00:07:43,500
is smart format

129
00:07:43,620 --> 00:07:50,819
yeah

130
00:07:45,319 --> 00:07:53,699
also it's supported for uh nested loops

131
00:07:50,819 --> 00:07:58,699
are also supported so let me just

132
00:07:53,699 --> 00:07:58,699
put uh I back here

133
00:07:59,639 --> 00:08:03,680
and convert it to J

134
00:08:07,860 --> 00:08:12,860
um I will say that

135
00:08:09,900 --> 00:08:12,860
it's equal to

136
00:08:13,080 --> 00:08:19,220
zero and

137
00:08:15,479 --> 00:08:19,220
Gmail is equal to

138
00:08:30,720 --> 00:08:34,399
um

139
00:08:31,800 --> 00:08:34,399
store

140
00:08:40,200 --> 00:08:45,899
yeah here's the result that we are

141
00:08:43,020 --> 00:08:48,240
currently exp that was expected it just

142
00:08:45,899 --> 00:08:51,740
tries to iterate

143
00:08:48,240 --> 00:08:51,740
over these values

144
00:08:52,339 --> 00:08:57,899
uh there are also other things uh for

145
00:08:55,800 --> 00:09:01,620
example you can evaluate the expressions

146
00:08:57,899 --> 00:09:03,779
this is not a uh this is uh just a

147
00:09:01,620 --> 00:09:05,940
simple command that evaluate the

148
00:09:03,779 --> 00:09:09,480
expressions for example if I want to add

149
00:09:05,940 --> 00:09:12,779
R is register to rbx register I will use

150
00:09:09,480 --> 00:09:15,779
the print command uh this is exactly the

151
00:09:12,779 --> 00:09:18,740
same as if if we want to use the print

152
00:09:15,779 --> 00:09:23,700
function not printf the print function

153
00:09:18,740 --> 00:09:25,680
and use it like at sign RX Plus at sign

154
00:09:23,700 --> 00:09:29,060
RP bits

155
00:09:25,680 --> 00:09:29,060
yeah this is the same

156
00:09:29,660 --> 00:09:36,019
uh also if you want to see other uh

157
00:09:39,200 --> 00:09:45,360
important parts like uh

158
00:09:42,779 --> 00:09:49,140
but the important student registers for

159
00:09:45,360 --> 00:09:52,080
example P name let's let me show you the

160
00:09:49,140 --> 00:09:54,839
client executing process name

161
00:09:52,080 --> 00:09:58,740
uh

162
00:09:54,839 --> 00:10:02,240
we will use it like a printf function

163
00:09:58,740 --> 00:10:06,120
which tries to

164
00:10:02,240 --> 00:10:10,700
run an S string or show the any string

165
00:10:06,120 --> 00:10:10,700
or the process name is

166
00:10:11,339 --> 00:10:18,720
and we will add a dollar

167
00:10:15,420 --> 00:10:22,920
to name here

168
00:10:18,720 --> 00:10:25,380
and execute this script

169
00:10:22,920 --> 00:10:27,899
yeah you can see that the current

170
00:10:25,380 --> 00:10:30,779
executing process name in the target

171
00:10:27,899 --> 00:10:35,160
debuggee

172
00:10:30,779 --> 00:10:39,839
is also a high producing that's HyperDbg

173
00:10:35,160 --> 00:10:43,440
CLI that exe but however it's as I

174
00:10:39,839 --> 00:10:47,579
mentioned in the S like the 16 bit wide

175
00:10:43,440 --> 00:10:51,920
uh string so everything after 16 which

176
00:10:47,579 --> 00:10:57,560
is uh truncated or removed

177
00:10:51,920 --> 00:11:01,019
and this way you can see the process

178
00:10:57,560 --> 00:11:04,380
oh shoot me

179
00:11:01,019 --> 00:11:07,920
uh so let's see another example for

180
00:11:04,380 --> 00:11:09,480
example I have a really good example in

181
00:11:07,920 --> 00:11:11,100
the

182
00:11:09,480 --> 00:11:14,600
um

183
00:11:11,100 --> 00:11:14,600
script Imaging

184
00:11:16,320 --> 00:11:19,399
box but

185
00:11:23,160 --> 00:11:26,760
okay uh

186
00:11:27,300 --> 00:11:32,640
in the previous stations we learned

187
00:11:29,339 --> 00:11:32,640
about uh

188
00:11:33,480 --> 00:11:37,040
setting breakpoints on a specific

189
00:11:35,640 --> 00:11:41,160
functions

190
00:11:37,040 --> 00:11:43,620
uh if you want to just know about the

191
00:11:41,160 --> 00:11:47,760
everything that I explained there slides

192
00:11:43,620 --> 00:11:50,640
are currently explained here also also

193
00:11:47,760 --> 00:11:52,920
explained here but you can always go

194
00:11:50,640 --> 00:11:55,880
through the documentation and see a more

195
00:11:52,920 --> 00:11:59,899
updated version of those as like

196
00:11:55,880 --> 00:11:59,899
uh there are also

197
00:12:00,060 --> 00:12:06,660
different examples that and also the

198
00:12:02,880 --> 00:12:08,820
updated version of uh the functions are

199
00:12:06,660 --> 00:12:11,160
also available here for example there

200
00:12:08,820 --> 00:12:14,180
are these functions this interlock

201
00:12:11,160 --> 00:12:18,779
function functions are also supported in

202
00:12:14,180 --> 00:12:20,480
HyperDbg so let's see some examples uh

203
00:12:18,779 --> 00:12:24,480
this is a really

204
00:12:20,480 --> 00:12:27,480
a good example if you want to use the

205
00:12:24,480 --> 00:12:29,160
script engine you can go and visit these

206
00:12:27,480 --> 00:12:31,260
examples

207
00:12:29,160 --> 00:12:32,279
I'm going to change the state of the

208
00:12:31,260 --> 00:12:36,839
machine

209
00:12:32,279 --> 00:12:39,240
or tracing the function calls uh

210
00:12:36,839 --> 00:12:42,480
this is a pretty good example let's just

211
00:12:39,240 --> 00:12:45,720
see together uh

212
00:12:42,480 --> 00:12:51,079
as is explained uh there is a function

213
00:12:45,720 --> 00:12:54,959
which is described in msdn by Microsoft

214
00:12:51,079 --> 00:12:57,060
anti-open file and this is uh the

215
00:12:54,959 --> 00:13:01,519
explanation of this kernel mode

216
00:12:57,060 --> 00:13:05,459
functions and we just want to simply

217
00:13:01,519 --> 00:13:10,820
see what file what file was trying to be

218
00:13:05,459 --> 00:13:14,459
opened by this NG open file from this uh

219
00:13:10,820 --> 00:13:17,000
functions we can see that this is the

220
00:13:14,459 --> 00:13:17,000
explanation

221
00:13:17,760 --> 00:13:25,920
the third argument to the function

222
00:13:22,200 --> 00:13:28,860
is object attributes or it's from a

223
00:13:25,920 --> 00:13:31,519
pointer to object underline attributes

224
00:13:28,860 --> 00:13:31,519
structure

225
00:13:31,560 --> 00:13:38,339
the object attribute as structure is

226
00:13:33,839 --> 00:13:40,980
defined like here uh this is the there

227
00:13:38,339 --> 00:13:43,680
there's a length root object name or

228
00:13:40,980 --> 00:13:45,959
other attributes here and if you want to

229
00:13:43,680 --> 00:13:47,760
see it's really a relative address we

230
00:13:45,959 --> 00:13:51,540
can easily use both to individually and

231
00:13:47,760 --> 00:13:55,100
HyperDbg uh the DT command if you

232
00:13:51,540 --> 00:13:55,100
remember from the previous parts

233
00:13:55,500 --> 00:14:00,000
um

234
00:13:57,019 --> 00:14:04,639
as you can see that there's a Unicode s

235
00:14:00,000 --> 00:14:08,880
string field here as an object name so

236
00:14:04,639 --> 00:14:12,899
its offset is 0x10 after the object

237
00:14:08,880 --> 00:14:15,480
attribute so currently we have we know

238
00:14:12,899 --> 00:14:19,160
that we want to go to the object

239
00:14:15,480 --> 00:14:22,380
attribute which is the third argument

240
00:14:19,160 --> 00:14:27,180
and uh in the talk in the object

241
00:14:22,380 --> 00:14:29,220
attribute we're gonna add 0x10 and reach

242
00:14:27,180 --> 00:14:32,760
to the object name

243
00:14:29,220 --> 00:14:36,180
which is the a Unicode string and in the

244
00:14:32,760 --> 00:14:38,820
Unicode string we have a length maximum

245
00:14:36,180 --> 00:14:42,180
length and the buffer itself the buffer

246
00:14:38,820 --> 00:14:46,680
itself is a Unicode exchange which you

247
00:14:42,180 --> 00:14:46,680
can see by using uh

248
00:14:47,160 --> 00:14:57,660
this operator I will explain it later

249
00:14:52,440 --> 00:15:00,899
but for now uh after reaching to the

250
00:14:57,660 --> 00:15:06,779
pointer or to the point of this Unicode

251
00:15:00,899 --> 00:15:09,180
a string we should uh find uh the buffer

252
00:15:06,779 --> 00:15:13,079
address and Jerry friends the buffer

253
00:15:09,180 --> 00:15:16,380
address as you as you know in the fast

254
00:15:13,079 --> 00:15:20,579
calling convention all the arguments are

255
00:15:16,380 --> 00:15:23,699
passed in rcx rdx r8 r9 and then

256
00:15:20,579 --> 00:15:26,399
everything is past the stack so we are

257
00:15:23,699 --> 00:15:28,639
gonna see the third argument so it's

258
00:15:26,399 --> 00:15:32,519
passed in the re

259
00:15:28,639 --> 00:15:35,220
rcx rdx and r8

260
00:15:32,519 --> 00:15:37,880
r9 and these are arguments that are

261
00:15:35,220 --> 00:15:40,920
passing Stacks so

262
00:15:37,880 --> 00:15:43,019
these arguments passed in are eight

263
00:15:40,920 --> 00:15:46,500
which is there so we

264
00:15:43,019 --> 00:15:48,300
we will first put a break point on

265
00:15:46,500 --> 00:15:50,940
anti-open file

266
00:15:48,300 --> 00:15:52,920
I'm not sure if I set the symbols or not

267
00:15:50,940 --> 00:15:55,320
but let's just try

268
00:15:52,920 --> 00:15:58,260
uh yeah everything is

269
00:15:55,320 --> 00:16:00,839
I will continue the debug normally and

270
00:15:58,260 --> 00:16:03,000
you can see that the break point zero x

271
00:16:00,839 --> 00:16:06,199
one is

272
00:16:03,000 --> 00:16:06,199
hit here

273
00:16:06,320 --> 00:16:18,060
no uh I will try to add uh here here is

274
00:16:13,079 --> 00:16:22,560
the pointer to object attribute I I will

275
00:16:18,060 --> 00:16:25,139
add 0x10 because we go we want to find

276
00:16:22,560 --> 00:16:28,260
the object name here

277
00:16:25,139 --> 00:16:32,060
and after that we reference it as a

278
00:16:28,260 --> 00:16:37,920
pointer by using the POI argument

279
00:16:32,060 --> 00:16:40,680
and then we will add 0x 8 here because

280
00:16:37,920 --> 00:16:43,440
we want to reach to the buffer

281
00:16:40,680 --> 00:16:45,000
I need a buffer we will print the

282
00:16:43,440 --> 00:16:50,399
address

283
00:16:45,000 --> 00:16:51,300
so let's see if it's true or not

284
00:16:50,399 --> 00:16:54,800
um

285
00:16:51,300 --> 00:16:54,800
yeah here is Dodgers

286
00:16:55,740 --> 00:16:58,880
as you can see that this is a Unicode

287
00:16:57,959 --> 00:17:03,000
address

288
00:16:58,880 --> 00:17:04,400
and it it's just name of a function so

289
00:17:03,000 --> 00:17:08,880
if

290
00:17:04,400 --> 00:17:12,720
the name of a file or name of a name of

291
00:17:08,880 --> 00:17:17,640
an object in the Windows so if I want to

292
00:17:12,720 --> 00:17:23,640
re continue seeing in a human readable

293
00:17:17,640 --> 00:17:23,640
format I will use the Ws identifier

294
00:17:24,959 --> 00:17:30,179
and evaluate these simple script

295
00:17:30,240 --> 00:17:34,980
um oh my god let's just swap

296
00:17:40,860 --> 00:17:46,740
yeah here it is here's the neck I will

297
00:17:43,559 --> 00:17:50,700
continue it again and you can see that

298
00:17:46,740 --> 00:17:53,760
this name is used again it's new and

299
00:17:50,700 --> 00:17:56,100
this then is used

300
00:17:53,760 --> 00:17:58,380
or let me

301
00:17:56,100 --> 00:18:01,820
add

302
00:17:58,380 --> 00:18:01,820
same is used

