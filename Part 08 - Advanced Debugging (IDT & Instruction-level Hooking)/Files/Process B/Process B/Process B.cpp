#include <stdio.h>

#include <windows.h>

#pragma section(".text")

//
// This shellcode is derived from: https://blackcloud.me/Win32-shellcode-3/
//

/*

	0:  31 c9                   xor    ecx,ecx
	2:  f7 e1                   mul    ecx
	4:  64 8b 41 30             mov    eax,DWORD PTR fs:[ecx+0x30]
	8:  8b 40 0c                mov    eax,DWORD PTR [eax+0xc]
	b:  8b 70 14                mov    esi,DWORD PTR [eax+0x14]
	e:  ad                      lods   eax,DWORD PTR ds:[esi]
	f:  96                      xchg   esi,eax
	10: ad                      lods   eax,DWORD PTR ds:[esi]
	11: 8b 58 10                mov    ebx,DWORD PTR [eax+0x10]
	14: 8b 53 3c                mov    edx,DWORD PTR [ebx+0x3c]
	17: 01 da                   add    edx,ebx
	19: 8b 52 78                mov    edx,DWORD PTR [edx+0x78]
	1c: 01 da                   add    edx,ebx
	1e: 8b 72 20                mov    esi,DWORD PTR [edx+0x20]
	21: 01 de                   add    esi,ebx
	23: 31 c9                   xor    ecx,ecx
	25: 41                      inc    ecx
	26: ad                      lods   eax,DWORD PTR ds:[esi]
	27: 01 d8                   add    eax,ebx
	29: 81 38 47 65 74 50       cmp    DWORD PTR [eax],0x50746547
	2f: 75 f4                   jne    0x25
	31: 81 78 04 72 6f 63 41    cmp    DWORD PTR [eax+0x4],0x41636f72
	38: 75 eb                   jne    0x25
	3a: 81 78 08 64 64 72 65    cmp    DWORD PTR [eax+0x8],0x65726464
	41: 75 e2                   jne    0x25
	43: 8b 72 24                mov    esi,DWORD PTR [edx+0x24]
	46: 01 de                   add    esi,ebx
	48: 66 8b 0c 4e             mov    cx,WORD PTR [esi+ecx*2]
	4c: 49                      dec    ecx
	4d: 8b 72 1c                mov    esi,DWORD PTR [edx+0x1c]
	50: 01 de                   add    esi,ebx
	52: 8b 14 8e                mov    edx,DWORD PTR [esi+ecx*4]
	55: 01 da                   add    edx,ebx
	57: 89 d5                   mov    ebp,edx
	59: 31 c9                   xor    ecx,ecx
	5b: 51                      push   ecx
	5c: 68 61 72 79 41          push   0x41797261
	61: 68 4c 69 62 72          push   0x7262694c
	66: 68 4c 6f 61 64          push   0x64616f4c
	6b: 54                      push   esp
	6c: 53                      push   ebx
	6d: ff d2                   call   edx
	6f: 68 6c 6c 61 61          push   0x61616c6c
	74: 66 81 6c 24 02 61 61    sub    WORD PTR [esp+0x2],0x6161
	7b: 68 33 32 2e 64          push   0x642e3233
	80: 68 55 73 65 72          push   0x72657355
	85: 54                      push   esp
	86: ff d0                   call   eax
	88: 68 6f 78 41 61          push   0x6141786f
	8d: 66 83 6c 24 03 61       sub    WORD PTR [esp+0x3],0x61
	93: 68 61 67 65 42          push   0x42656761
	98: 68 4d 65 73 73          push   0x7373654d
	9d: 54                      push   esp
	9e: 50                      push   eax
	9f: ff d5                   call   ebp
	a1: 83 c4 10                add    esp,0x10
	a4: 31 d2                   xor    edx,edx
	a6: 31 c9                   xor    ecx,ecx
	a8: 52                      push   edx
	a9: 68 50 77 6e 64          push   0x646e7750
	ae: 89 e7                   mov    edi,esp
	b0: 52                      push   edx
	b1: 68 59 65 73 73          push   0x73736559
	b6: 89 e1                   mov    ecx,esp
	b8: 52                      push   edx
	b9: 57                      push   edi
	ba: 51                      push   ecx
	bb: 52                      push   edx
	bc: ff d0                   call   eax
	be: 83 c4 28                add    esp,0x28

*/

__declspec(allocate(".text")) char messagebox_shellcode[] =
"\x31\xc9\xf7\xe1\x64\x8b\x41\x30\x8b\x40"
"\x0c\x8b\x70\x14\xad\x96\xad\x8b\x58\x10"
"\x8b\x53\x3c\x01\xda\x8b\x52\x78\x01\xda"
"\x8b\x72\x20\x01\xde\x31\xc9\x41\xad\x01"
"\xd8\x81\x38\x47\x65\x74\x50\x75\xf4\x81"
"\x78\x04\x72\x6f\x63\x41\x75\xeb\x81\x78"
"\x08\x64\x64\x72\x65\x75\xe2\x8b\x72\x24"
"\x01\xde\x66\x8b\x0c\x4e\x49\x8b\x72\x1c"
"\x01\xde\x8b\x14\x8e\x01\xda\x89\xd5\x31"
"\xc9\x51\x68\x61\x72\x79\x41\x68\x4c\x69"
"\x62\x72\x68\x4c\x6f\x61\x64\x54\x53\xff"
"\xd2\x68\x6c\x6c\x61\x61\x66\x81\x6c\x24"
"\x02\x61\x61\x68\x33\x32\x2e\x64\x68\x55"
"\x73\x65\x72\x54\xff\xd0\x68\x6f\x78\x41"
"\x61\x66\x83\x6c\x24\x03\x61\x68\x61\x67"
"\x65\x42\x68\x4d\x65\x73\x73\x54\x50\xff"
"\xd5\x83\xc4\x10\x31\xd2\x31\xc9\x52\x68"
"\x50\x77\x6e\x64\x89\xe7\x52\x68\x59\x65"
"\x73\x73\x89\xe1\x52\x57\x51\x52\xff\xd0"
"\x83\xc4\x28";

/*

	0:  9c                      pushf
	1:  50                      push   eax
	2:  51                      push   ecx
	3:  52                      push   edx
	4:  53                      push   ebx
	5:  54                      push   esp (nop)
	6:  55                      push   ebp
	7:  56                      push   esi
	8:  57                      push   edi

*/

__declspec(allocate(".text")) char save_registers_state[] =
"\x9c\x50\x51\x52\x53\x90\x55\x56\x57";

/*

	0:  5f                      pop    edi
	1:  5e                      pop    esi
	2:  5d                      pop    ebp
	3:  5c                      pop    esp (nop)
	4:  5b                      pop    ebx
	5:  5a                      pop    edx
	6:  59                      pop    ecx
	7:  58                      pop    eax
	8:  9d                      popf

*/

__declspec(allocate(".text")) char restore_registers_state[] =
"\x5f\x5e\x5d\x90\x5b\x5a\x59\x58\x9d";

/*

	0:  90                      nop
	00000001 <waiting>:
	1:  90                      nop
	2:  eb fd                   jmp    1 <waiting>

*/

__declspec(allocate(".text")) char idle_nop_loop[] =
"\x90\x90\xeb\xfd";


int main() {

	printf("process id: 0x%x\n", GetCurrentProcessId());

	UINT32 ShellcodeLen = strlen(messagebox_shellcode) + strlen(idle_nop_loop);


	printf("shellcode length: %d\n", ShellcodeLen);

	LPVOID AllocBuf = VirtualAlloc(0, 4096, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	printf("shellcode address: 0x%x\n", AllocBuf);

	memcpy(AllocBuf, messagebox_shellcode, strlen(messagebox_shellcode));
	memcpy((void*)((UINT32)AllocBuf + strlen(messagebox_shellcode)), idle_nop_loop, strlen(idle_nop_loop));

	((void(*)())AllocBuf)();

}