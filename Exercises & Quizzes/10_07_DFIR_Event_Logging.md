## Digital Forensics and Incident Response - DFIR

### Exercise List:

1. **Forward events**
   Create an output (a TCP socket, or a File) and open it by using the 'output' command (you can use the sample program available at the repository linked below).
   
2. **Forward system-calls**
   Use the 'output' option of the '!syscall' (or any other events) to forward the results of a script to the TCP socket or the file.

### Links:

* [Event forwarding](https://docs.hyperdbg.org/tips-and-tricks/misc/event-forwarding)

* [output (create output source for event forwarding)](https://docs.hyperdbg.org/commands/debugging-commands/output)

* [!syscall, !syscall2 (hook system-calls)](https://docs.hyperdbg.org/commands/extension-commands/syscall)

* [event-forwarding-examples](https://github.com/HyperDbg/event-forwarding-examples)

* [Using HyperDbg as a #DFIR tool - Event Forwarding](https://www.youtube.com/watch?v=tyapdCEZtic)