## Reading & Modifying Memory

### Exercise List:

1. **Read memory**
   Read the memory at the 'nt!Kd\_DEFAULT\_Mask'.

2. **Modify memory**
   Edit the memory at the 'nt!Kd\_DEFAULT\_Mask', set the '0xffffffff' to it.

### Links:

* [db, dc, dd, dq (read virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/d)

* [eb, ed, eq (edit virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/e)