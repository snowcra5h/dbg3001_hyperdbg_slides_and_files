## System Call Return Hooks

### Exercise List:

1. **Sysret interception**
   Open 'notepad.exe' and get the process id, then intercept all of the SYSRET instructions (or returns from the system-calls).

2. **Print the RAX register**
   Print the 'RAX' register, so that once the 'notepad.exe' executes a SYSRET instruction (in the kernel), a message with the corresponding 'RAX' register should be shown.

### Links:

* [Part 6 - Files](https://url.hyperdbg.org/ost2/part-6-files)

* [!sysret, !sysret2 (hook SYSRET instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/sysret)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)