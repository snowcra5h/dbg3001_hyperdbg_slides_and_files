## Examining CPU

### Exercise List:

1. **View CPU features**
   Use the 'cpu' command to see the CPU features.

### Links:

* [cpu (check cpu supported technologies)](https://docs.hyperdbg.org/commands/debugging-commands/cpu)