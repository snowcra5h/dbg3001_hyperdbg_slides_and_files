## Hands-on Recap

### Exercise List:

1. **Compile the projects**
   Compile the 'Process A' and the 'Process B' projects. Find it in the git repo.
   
2. **Intercept the context of the target process**
   Intercept the context of the target process by using the '!interrupt' command. (As shown in the video).
   
3. **Modify the page-table**
   Modify the page-table entries related to the 'Process A' and the 'Process B' shellcode and show the message box. (As shown in the video).
   
4. **Restore the state**
   Restore the state of 'Process A' to its normal execution (avoid crash). (As shown in the video).

### Links:

* [Process A](https://url.hyperdbg.org/ost2/process-a)

* [Process B](https://url.hyperdbg.org/ost2/process-b)

* [!interrupt (hook external device interrupts)](https://docs.hyperdbg.org/commands/extension-commands/interrupt)

* [!interrupt (hook external device interrupts)](https://docs.hyperdbg.org/commands/extension-commands/interrupt)

* [!pte (display page-level address and entries)](https://docs.hyperdbg.org/commands/extension-commands/pte)

* [events (show and modify active/disabled events)](https://docs.hyperdbg.org/commands/debugging-commands/events)

* [r (read or modify registers)](https://docs.hyperdbg.org/commands/debugging-commands/r)

* [db, dc, dd, dq (read virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/d)

* [eb, ed, eq (edit virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/e)

* [pause](https://docs.hyperdbg.org/commands/scripting-language/functions/debugger/pause)