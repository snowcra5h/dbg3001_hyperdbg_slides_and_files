## CPUID Instruction Hooking

### Exercise List:

1. **Print all the CPUIDs**
   Print all the CPUID instructions that are running in your system, along with CPUID code (the '$context' pseudo-register), and show the process name and the process id.
   
2. **Modify the CPUIDs**
   Modify the CPUID results and try to fool the CPU-Z program that the VT-x feature is not available in your processor. You can also see the modifications by using the 'cpuid\_test' project.
   
3. **Change vendor**
   Fool the CPU-Z program about your processor and change the vendor from Intel to AMD. (As shown in the video).

### Links:

* [CPU-Z | Softwares | CPUID](https://www.cpuid.com/softwares/cpu-z.html)

* [CPUID](https://en.wikipedia.org/wiki/CPUID)

* [cpuid_test](https://url.hyperdbg.org/ost2/cpuid_test)

* [CPUID.ds](https://url.hyperdbg.org/ost2/cpuid_ds)

* [!cpuid (hook CPUID instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/cpuid)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [Pseudo-registers](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations#pseudo-registers)