## Ignoring Events

### Exercise List:

1. **Block a special system-call**
   Write a script using the '!syscall' command to ignore the 'ntdll!NtCreateFile' system-calls of a 'notepad.exe' process and return the 'STATUS\_ACCESS\_DENIED' in the 'RAX' register. (as shown in the video).
   
2. **Compile the program**
   Compile the 'write\_ignore' program (find the source code in the repo).
   
3. **Prevent incrementation**
   Write a script and ignore the memory modification of the target incrementing variable.

4. **Compile the program**
   Compile the 'hello\_world' program (find the source code in the repo).
   
5. **Block the execution**
   Find the main module's starting point using the 'lm' command. Block the execution of the main module using the event-short circuiting mechanism in the '!monitor' command (as shown in the video). 

### Links:

* [write_ignore](https://url.hyperdbg.org/ost2/write_ignore)

* [hello_world](https://url.hyperdbg.org/ost2/hello_world)

* [Part 10 - Files](https://url.hyperdbg.org/ost2/part-10-files)

* [!syscall, !syscall2 (hook system-calls)](https://docs.hyperdbg.org/commands/extension-commands/syscall)

* [!monitor (monitor read/write/execute to a range of memory)](https://docs.hyperdbg.org/commands/extension-commands/monitor)

* [lm (view loaded modules)](https://docs.hyperdbg.org/commands/debugging-commands/lm)

* [Event short-circuiting](https://docs.hyperdbg.org/tips-and-tricks/misc/event-short-circuiting)

* [event_sc](https://docs.hyperdbg.org/commands/scripting-language/functions/events/event_sc)