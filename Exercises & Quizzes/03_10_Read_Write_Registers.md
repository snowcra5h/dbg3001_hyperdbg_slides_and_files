## Reading & Modifying Registers

### Exercise List:

1. **Read all registers**
   Read all of the general-purpose registers using the 'r' command.

2. **Read a specific register**
   Read and modify the 'RCX' register using the 'r' command.

3. **View registers in different forms**
   Use the '.formats' command to view the 'RAX' register in different formats.

### Links:

* [r (read or modify registers)](https://docs.hyperdbg.org/commands/debugging-commands/r)

* [.formats (show number formats)](https://docs.hyperdbg.org/commands/meta-commands/.formats)