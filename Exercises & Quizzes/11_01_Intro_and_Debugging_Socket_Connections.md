## Intro & Debugging Socket Connections

### Exercise List:

1. **Compile the 'simple\_send' project**
   Compile the 'simple\_send' project.
   
2. **Find the system-call**
   Find the system-call relating to the sending packets to the network.
   
3. **Check the parameters**
   Find the network IP address and the Port number. (shown in the video).

4. **Sniff network connections**
   Sniff the network connection (IP & Port) using the script written in the video.

5. **Sniff the network data**
   Sniff the network data using the script written in the video.

### Links:

* [simple_send](https://url.hyperdbg.org/ost2/simple-send)

* [Part 11 - Files](https://url.hyperdbg.org/ost2/part-11-files)

* [Dr. Memory](https://drmemory.org)

* [AFD_CONNECT](https://chromium.googlesource.com/external/github.com/DynamoRIO/drmemory/+/refs/heads/master/drsyscall/drsyscall_windows.c#3031)

* [TCP Connection Analysis with Tycho](https://www.cyberus-technology.de/posts/network-analysis-with-tycho/)

* [!syscall, !syscall2 (hook system-calls)](https://docs.hyperdbg.org/commands/extension-commands/syscall)

* [!epthook (hidden hook with EPT - stealth breakpoints)](https://docs.hyperdbg.org/commands/extension-commands/epthook)

* [db, dc, dd, dq (read virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/d)