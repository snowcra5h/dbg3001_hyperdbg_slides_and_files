## Intro & VT-x & Page Tables & Extended Page Tables

### Exercise List:

1. **Convert the virtual address to the physical address**
   Use the '!va2pa' command to convert the virtual address of the 'nt!ExAllocatePoolWithTag' function to the corresponding physical address.
   
2. **View the physical address**
   Use the '!db' command to see the physical address derived from the '!va2pa' command.
   
3. **Disassemble the physical address**
   Use the '!u' command to disassemble the physical address of the 'nt!ExAllocatePoolWitTag', and compare the results with the 'u' command on the virtual address of this function.
   
4. **Compile the ExecCodeStack project**
   Find the source code of the 'ExecCodeStack' project in the git repo and compile it.
   
5. **Download symbols**
   Perform the page-level (attribute) modification applied to the 'ExecCodeStack' project (As shown in the video).

### Links:

* [PAGING ON INTEL X86-64](https://www.iaik.tugraz.at/teaching/materials/os/tutorials/paging-on-intel-x86-64/)

* [!pte (display page-level address and entries)](https://docs.hyperdbg.org/commands/extension-commands/pte)

* [!va2pa (convert a virtual address to physical address)](https://docs.hyperdbg.org/commands/extension-commands/va2pa)

* [!db, !dc, !dd, !dq (read physical memory)](https://docs.hyperdbg.org/commands/extension-commands/d)

* [!u, !u64, !u2, !u32 (disassemble physical address)](https://docs.hyperdbg.org/commands/extension-commands/u)

* [u, u64, u2, u32 (disassemble virtual address)](https://docs.hyperdbg.org/commands/debugging-commands/u)