## Structures

### Exercise List:

1. **Dump enumerations**
   Extract the C enum of the 'nt!POWER\_ACTION' from symbols using the 'struct' command.

2. **Dump enumerations**
   Extract the C structure of the 'nt!\_EPROCESS' and the 'nt!\_KPROCESS' from symbols using the 'struct' command.
   
3. **Dump all of the enums and structures from the symbols**
   Extract all of the C enums and structures from symbols to a file using the 'struct' command.

### Links:

* [Part 3 - Files](https://url.hyperdbg.org/ost2/part-3-files)

* [struct (make structures, enums, data types from symbols)](https://docs.hyperdbg.org/commands/debugging-commands/struct)