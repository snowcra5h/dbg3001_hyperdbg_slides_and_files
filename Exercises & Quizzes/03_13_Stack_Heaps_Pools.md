## Stacks, Heaps, and Pools

### Exercise List:

1. **Read the stack trace**
   Use the 'kq' command to view the current stack frames.

2. **Map PCB memory to structure**
   Use the '.process' command to view the current PCB (Process Control Block) or the 'nt!\_EPROCESS' of the current process and map it to the 'nt!\_EPROCESS' command by using the 'dt' command.

### Links:

* [k, kd, kq (display stack backtrace)](https://docs.hyperdbg.org/commands/debugging-commands/k)

* [dt (display and map virtual memory to structures)](https://docs.hyperdbg.org/commands/debugging-commands/dt)