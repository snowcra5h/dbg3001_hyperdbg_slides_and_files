## Breakpoints

### Exercise List:

1. **Set breakpoints**
   Set two breakpoints, one on the 'nt!ExAllocatePoolWithTag' and one on the 'nt!NtCreateFile'.

2. **See list of breakpoints**
   Use the 'bl' command to view the list of breakpoints.

3. **Disable breakpoints**
   Use the 'bd' command to disable the breakpoints and again check the list of breakpoints using the 'bl' command.

4. **Enable breakpoints**
   Use the 'be' command to enable the breakpoints and again check the list of breakpoints using the 'bl' command.
   
5. **Clear breakpoints**
   Use the 'bc' command to clear the breakpoints and again check the list of breakpoints using the 'bl' command.
   
### Links:

* [bp (set breakpoint)](https://docs.hyperdbg.org/commands/debugging-commands/bp)

* [bl (list breakpoints)](https://docs.hyperdbg.org/commands/debugging-commands/bl)

* [be (enable breakpoints)](https://docs.hyperdbg.org/commands/debugging-commands/be)

* [bd (disable breakpoints)](https://docs.hyperdbg.org/commands/debugging-commands/bd)

* [bc (clear and remove breakpoints)](https://docs.hyperdbg.org/commands/debugging-commands/bc)