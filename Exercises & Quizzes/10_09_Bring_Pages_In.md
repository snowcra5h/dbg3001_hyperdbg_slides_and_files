## Bring Pages In

### Exercise List:

1. **Start the process**
   Compile the 'SimpleMessageBox' program (find the source code in the repo), and start this process by using the '.start' command.

2. **Reload the symbol**
   Reload the symbol by using the '.sym' command and passing its process id.

3. **Bring the page in**
   Use the '.pagein' command to make the page table residing in the 'user32!MessageBoxA' valid in the target process's context, and then use the 'bp' command to put a breakpoint in the target function. Once, the breakpoint is triggered, modify the parameters (the message text string).

### Links:

* [SimpleMessageBox](https://url.hyperdbg.org/ost2/simple_message_box)

* [.start (start a new process)](https://docs.hyperdbg.org/commands/meta-commands/.start)

* [.sym (load pdb symbols)](https://docs.hyperdbg.org/commands/meta-commands/.sym)

* [bp (set breakpoint)](https://docs.hyperdbg.org/commands/debugging-commands/bp)

* [.pagein (bring the page into the RAM)](https://docs.hyperdbg.org/commands/meta-commands/.pagein)