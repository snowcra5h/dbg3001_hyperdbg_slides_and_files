## Script Engine Functions

### Exercise List:

1. **Load symbol path**
   Use the '.sympath' command to set the symbol path.

2. **Download symbols**
   Download the symbols using the '.sym download' command.

3. **Examine symbols**
   Examine the downloaded symbols by using the 'x' command (Use the wildcard syntax or the '\*').

### Links:

* [Functions](https://docs.hyperdbg.org/commands/scripting-language/functions)

* [.sympath (set the symbol server)](https://docs.hyperdbg.org/commands/meta-commands/.sympath)

* [.sym (load pdb symbols)](https://docs.hyperdbg.org/commands/meta-commands/.sym)

* [x (examine symbols and find functions and variables address)](https://docs.hyperdbg.org/commands/debugging-commands/x)