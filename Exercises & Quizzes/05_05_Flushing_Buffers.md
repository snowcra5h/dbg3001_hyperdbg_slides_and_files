## Flushing Buffers

### Exercise List:

1. **Connect to the debugger in VMI Mode (Local Debugging)**
   Connect to the HyperDbg in the VMI Mode.

2. **Set EPT Hooks**
   Set an '!epthook' to the 'nt!ExAllocatePoolWithTag' that prints the parameters to this function.

3. **Clear events**
   Clear the '!epthook' event using the 'events' command.

4. **Flush the buffer**
   As you can see, the buffers keep coming to the user-mode even when you removed (clear) the event. Use the 'flush' command to remove those buffers.

### Links:

* [VMI Mode](https://docs.hyperdbg.org/using-hyperdbg/prerequisites/operation-modes#vmi-mode)

* [!epthook (hidden hook with EPT - stealth breakpoints)](https://docs.hyperdbg.org/commands/extension-commands/epthook)

* [events (show and modify active/disabled events)](https://docs.hyperdbg.org/commands/debugging-commands/events)

* [flush (remove pending kernel buffers and messages)](https://docs.hyperdbg.org/commands/debugging-commands/flush)