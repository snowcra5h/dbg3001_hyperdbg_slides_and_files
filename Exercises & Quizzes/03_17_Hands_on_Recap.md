## Hands-on Recap

### Exercise List:

1. **Compile simple\_send project**
   Compile the 'simple\_send' project (find it in the files in the git repo).

2. **Trace the instructions**
   Trace the instructions using the 'i' command and find the target system-call.
   
3. **Examine the parameters of the system-call**
   Use the 'dq', and the 'dc' command to view the parameters passed for the system-call.

4. **Track instructions**
   Use the '!track' command to trace the system-calls from user-mode to kernel-mode.

### Links:

* [simple_send](https://url.hyperdbg.org/ost2/part-3-simple-send)

* [i (instrumentation step-in)](https://docs.hyperdbg.org/commands/debugging-commands/i)

* [db, dc, dd, dq (read virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/d)

* [!track (track and map function calls and returns to the symbols)](https://docs.hyperdbg.org/commands/extension-commands/track)