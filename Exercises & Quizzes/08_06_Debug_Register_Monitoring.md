## Mov to Debug Register Monitoring

### Exercise List:

1. **Print any use of Hardware Debug Registers**
   Print the 'RIP' register along with the process name and the process id of any instances of Hardware Deubg Registers modification. You can use other debuggers like 'x64dbg' to set a break on the access breakpoint on a custom address and view the event in HyperDbg.

### Links:

* [x64dbg](https://x64dbg.com)

* [!dr (hook access to debug registers)](https://docs.hyperdbg.org/commands/extension-commands/dr)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [Pseudo-registers](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations#pseudo-registers)