## Userspace debugging

### Exercise List:

1. **Start the user-mode process**
   Start a specific user-mode process like 'notepad.exe'.

2. **Restart the user-mode process**
   Restart the target process.

3. **Kill the user-mode process**
   Kill the target process.

### Links:

* [.start (start a new process)](https://docs.hyperdbg.org/commands/meta-commands/.start)

* [.restart (restart the process)](https://docs.hyperdbg.org/commands/meta-commands/.restart)

* [.kill (terminate the process)](https://docs.hyperdbg.org/commands/meta-commands/.kill)