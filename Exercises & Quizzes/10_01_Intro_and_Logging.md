## Intro, and Log Open & Close

### Exercise List:

1. **Save logs**
   Use the '.logopen' command to save the logs.

2. **Close logs**
   Use the '.logclose' command to close the log file.

### Links:

* [.logopen (open log file)](https://docs.hyperdbg.org/commands/meta-commands/.logopen)

* [.logclose (close log file)](https://docs.hyperdbg.org/commands/meta-commands/.logclose)