!epthook nt!DbgBreakPointWithStatus script {

	@rip = poi(@rsp); // pop the return address from stack
	@rsp = @rsp + 8; // adjust the stack
	
	printf("nt!DbgBreakPointWithStatus is ignored.\n");

}


# ------------------------------------------------------

03:	fffff8062162d1c0 nt!KiBreakpointTrap


!exception 03 script {

	event_sc(1);
	printf("breakpoint (int3) is ignored.\n");
}

# ------------------------------------------------------

0: kHyperDbg> lm km m kd

fffff806`26260000       47000   kdnet.dll                       \SystemRoot\system32\kdnet.dll
fffff806`26200000       5d000   kd_02_8086.dll                  \SystemRoot\system32\kd_02_8086.dll
fffff806`2b400000       f000    kdnic.sys                       \SystemRoot\System32\drivers\kdnic.sys


# ------------------------------------------------------

!epthook nt!KiBreakpointTrap


# ------------------------------------------------------



!msrread c0000082 script {

	//
	// Fill the EDX:EAX
	//
	@rdx = f0f0f0f0;
	@rax = 10203040;
	
	event_sc(1);
	
	printf("%llx is read!\n", @rcx);


}


# ------------------------------------------------------


PROCESS ffffc105a506b080
    SessionId: 1  Cid: 0684    Peb: cb3cb72000  ParentCid: 08bc
    DirBase: 0c76a000  ObjectTable: ffffaf8658518c00  HandleCount: 816.
    Image: OneDrive.exe



0: kd> dq ffffc105a506b080+28
ffffc105`a506b0a8  00000000`0c76a000 ffffc105`a45d9378
ffffc105`a506b0b8  ffffc105`a78da378 00000000`00000000
ffffc105`a506b0c8  00000000`00000000 00000000`00200001
ffffc105`a506b0d8  00000000`0000003f 00000000`00000000
ffffc105`a506b0e8  00000000`00000000 00000000`00000000
ffffc105`a506b0f8  00000000`00000000 00000000`00000000
ffffc105`a506b108  00000000`00000000 00000000`00000000
ffffc105`a506b118  00000000`00000000 00000000`00000000


# ------------------------------------------------------

!monitor rw ffffc105`a506b0a8 ffffc105`a506b0a8+7 script {
	if (@rip != nt!SwapContext+0x25b && @rip != nt!KiStackAttachProcess+0x1b5 && @rip != nt!KiDetachProcess+0x184) {
		pause();
	}
}


# ------------------------------------------------------

PROCESS ffffc105a50f0080
    SessionId: 1  Cid: 0db8    Peb: 394328e000  ParentCid: 133c
    DirBase: 1b2c41000  ObjectTable: 00000000  HandleCount:   0.
    Image: OneDrive.exe


!epthook kdnet!KdSendPacket script {


	printf("\n------------------------------------------------------------------------\n");
	printf("PacketType: %llx, FirstBuffer: %llx, SecondBuffer: %llx, KdContext: %llx\n", @rcx, @rdx, @r8, @r9);
	
	byte_len = 100;
	
	for (i = 0; i <= byte_len;i++) {
		
		if (dq(@rdx + i) == ffffc105a50f0080) {
			pause();
		}
	}
		
	for (i = 0; i <= byte_len;i++) {
		
		if (dq(@r8 + i) == ffffc105a50f0080) {
			pause();
		}
	}
}

# ------------------------------------------------------

!epthook nt!KdpReadVirtualMemory


# ------------------------------------------------------

0: kHyperDbg> !track
┌── nt!KdpCopyMemoryChunks (fffff806`21c9c0a0)
│  ┌── nt!MmDbgCopyMemory (fffff806`214b4d7c)
│  │  ┌── nt!MiDbgCopyMemory (fffff806`214b4e24)
│  │  │  ┌── nt!KeIsUserVaAccessAllowed (fffff806`21484250)
│  │  │  └── nt!KeIsUserVaAccessAllowed+0x1e (fffff806`2148426e)
│  │  │  ┌── nt!MmIsAddressValidEx (fffff806`214b4f80)
│  │  │  └── nt!MmIsAddressValidEx+0x12f (fffff806`214b50af)
│  │  │  ┌── nt!MiCopyFromUntrustedMemory (fffff806`214b50bc)
│  │  │  │  ┌── nt!_security_check_cookie (fffff806`215e3750)
│  │  │  │  └── nt!_security_check_cookie+0x14 (fffff806`215e3764)
│  │  │  └── nt!MiCopyFromUntrustedMemory+0x144 (fffff806`214b5200)
│  │  └── nt!MiDbgCopyMemory+0x10a (fffff806`214b4f2e)
│  └── nt!MmDbgCopyMemory+0x9d (fffff806`214b4e19)
│  ┌── nt!MmDbgCopyMemory (fffff806`214b4d7c)
│  │  ┌── nt!MiDbgCopyMemory (fffff806`214b4e24)
│  │  │  ┌── nt!KeIsUserVaAccessAllowed (fffff806`21484250)
│  │  │  └── nt!KeIsUserVaAccessAllowed+0x1e (fffff806`2148426e)
│  │  │  ┌── nt!MmIsAddressValidEx (fffff806`214b4f80)
│  │  │  └── nt!MmIsAddressValidEx+0x12f (fffff806`214b50af)
│  │  │  ┌── nt!MiCopyFromUntrustedMemory (fffff806`214b50bc)
│  │  │  │  ┌── nt!_security_check_cookie (fffff806`215e3750)
│  │  │  │  └── nt!_security_check_cookie+0x14 (fffff806`215e3764)
│  │  │  └── nt!MiCopyFromUntrustedMemory+0x144 (fffff806`214b5200)


OneDrive
4F6E654472697665


MmDbgCopyMemory

MiDbgCopyMemory

MiCopyFromUntrustedMemory


!epthook nt!MmDbgCopyMemory script {
	if (db(@rcx) == 0x4f && db(@rcx + 1) == 0x00 &&  db(@rcx + 2) == 0x6E ) {
		printf("nt!MmDbgCopyMemory is called form RIP: %llx, Address: %llx, And the string is : %ws\n", @rip, @rcx, @rcx);
	}
}

!epthook nt!MiDbgCopyMemory script {
	if (db(@rcx) == 0x4f && db(@rcx + 1) == 0x00 &&  db(@rcx + 2) == 0x6E ) {
		printf("nt!MmDbgCopyMemory is called form RIP: %llx, Address: %llx, And the string is : %ws\n", @rip, @rcx, @rcx);
	}
}
!epthook nt!MiCopyFromUntrustedMemory script {
	if (db(@rcx) == 0x4f && db(@rcx + 1) == 0x00 &&  db(@rcx + 2) == 0x6E ) {
		printf("nt!MmDbgCopyMemory is called form RIP: %llx, Address: %llx, And the string is : %ws\n", @rip, @rcx, @rcx);
	}
}


# ------------------------------------------------------
