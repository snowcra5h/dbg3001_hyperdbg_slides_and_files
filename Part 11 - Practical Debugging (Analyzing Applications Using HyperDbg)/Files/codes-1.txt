!syscall pid 0x16fc script {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x12007) {
		
			pause();
		}

	}
}

# ---------------------------------------------------------------

!syscall pid 0x16fc script {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x12007) {
		
			port_num_value_high = db(poi(rsp+38)+1a);
			port_num_value_low = db(poi(rsp+38)+1b);
			
			port_num = 0;
			port_num = port_num_value_high << 0n8 | port_num_value_low;
			
			printf("Port address is : %d \n", port_num);
			
			part0 = db(poi(@rsp+38) + 1c);
			part1 = db(poi(@rsp+38) + 1d);
			part2 = db(poi(@rsp+38) + 1e);
			part3 = db(poi(@rsp+38) + 1f);
			
			part0 = part0 << 0n24;
			part1 = part1 << 0n16;
			part2 = part2 << 0n8;
			part3 = part3 << 0n0;
			
			ip_addr = part0 | part1 | part2 | part3;
			
			printf("Ip address is : %x\n", ip_addr);
			
			
			
		}

	}
}

# ----------------------------------------------------------------------

? {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x12007) {
		
			port_num_value_high = db(poi(rsp+38)+1a);
			port_num_value_low = db(poi(rsp+38)+1b);
			
			port_num = 0;
			port_num = port_num_value_high << 0n8 | port_num_value_low;
			
			
			printf("Port address is : %d \n", port_num);
			
		}

	}

}

# -----------------------------------------------------------------------
? {
	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x12007) {
		
			port_num_value_high = db(poi(rsp+38)+1a);
			port_num_value_low = db(poi(rsp+38)+1b);
			
			port_num = 0;
			port_num = port_num_value_high << 0n8 | port_num_value_low;
			
			printf("Port address is : %d \n", port_num);
			
			part0 = db(poi(@rsp+38) + 1c);
			part1 = db(poi(@rsp+38) + 1d);
			part2 = db(poi(@rsp+38) + 1e);
			part3 = db(poi(@rsp+38) + 1f);
			
			part0 = part0 << 0n24;
			part1 = part1 << 0n16;
			part2 = part2 << 0n8;
			part3 = part3 << 0n0;
			
			ip_addr = part0 | part1 | part2 | part3;
			
			printf("Ip address is : %x\n", ip_addr);
			
			printf("Ip address is : %d.%d.%d.%d\n", (ip_addr & 0xff000000) >> 0n24, (ip_addr & 0x00ff0000) >> 0n16, (ip_addr & 0x0000ff00) >> 0n8, (ip_addr & 0x000000ff));
			
			
		}

	}
}


# ----------------------------------------------------------------------

!syscall pid 0x16fc script {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x12007) {
		
			port_num_value_high = db(poi(rsp+38)+1a);
			port_num_value_low = db(poi(rsp+38)+1b);
			
			port_num = 0;
			port_num = port_num_value_high << 0n8 | port_num_value_low;
			
			//printf("Port address is : %d \n", port_num);
			
			part0 = db(poi(@rsp+38) + 1c);
			part1 = db(poi(@rsp+38) + 1d);
			part2 = db(poi(@rsp+38) + 1e);
			part3 = db(poi(@rsp+38) + 1f);
			
			part0 = part0 << 0n24;
			part1 = part1 << 0n16;
			part2 = part2 << 0n8;
			part3 = part3 << 0n0;
			
			ip_addr = part0 | part1 | part2 | part3;
			
			// printf("Ip address is : %x\n", ip_addr);
			
			printf("Ip address is : %d.%d.%d.%d:%d\n", (ip_addr & 0xff000000) >> 0n24, (ip_addr & 0x00ff0000) >> 0n16, (ip_addr & 0x0000ff00) >> 0n8, (ip_addr & 0x000000ff),port_num );
			
			
		}

	}

}

# ----------------------------------------------------------------------

!syscall script {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x12007) {
		
			port_num_value_high = db(poi(rsp+38)+1a);
			port_num_value_low = db(poi(rsp+38)+1b);
			
			port_num = 0;
			port_num = port_num_value_high << 0n8 | port_num_value_low;
			
			//printf("Port address is : %d \n", port_num);
			
			part0 = db(poi(@rsp+38) + 1c);
			part1 = db(poi(@rsp+38) + 1d);
			part2 = db(poi(@rsp+38) + 1e);
			part3 = db(poi(@rsp+38) + 1f);
			
			part0 = part0 << 0n24;
			part1 = part1 << 0n16;
			part2 = part2 << 0n8;
			part3 = part3 << 0n0;
			
			ip_addr = part0 | part1 | part2 | part3;
			
			// printf("Ip address is : %x\n", ip_addr);
			
			printf("Process Id: %x, Process name: %s ====> Connects to Ip address: %d.%d.%d.%d:%d\n", $pid, $pname, (ip_addr & 0xff000000) >> 0n24, (ip_addr & 0x00ff0000) >> 0n16, (ip_addr & 0x0000ff00) >> 0n8, (ip_addr & 0x000000ff), port_num);
			
		}
	}
}


# ----------------------------------------------------------------------

!syscall pid 0x16fc script {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x1201F) {
		
			pause();

		}
	}
}


# ----------------------------------------------------------------------

!syscall pid 0x16fc script {

	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x1201F) {
		
			buffer_len = dq(poi(poi(@rsp+38)));
			printf("Packet buffer is: %s\n", dq(poi(poi(@rsp+38))+8));
			
			for (i = 0; i< buffer_len;i++) {
				printf(" %x", db(poi(poi(@rsp+38))+8)+i));
			}

		}
	}
}


# ----------------------------------------------------------------------

!syscall pid 0x16fc script {
	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x1201F) {
		
			buffer_len = dq(poi(poi(@rsp+38)));
			
			
			printf("\n\n===========================================================================\n");
			printf("Packet buffer is: %s\n", dq(poi(poi(@rsp+38))+8));
			
			for (i = 0; i < buffer_len;i++) {
				printf(" %x", db(poi(poi(poi(@rsp+38))+8)+i));
			}

		}
	}
}

# ----------------------------------------------------------------------

!syscall script {
	if (@rax == 0x7) {
	
		if (dd(@rsp + 30) == 0x1201F) {
		
			buffer_len = dq(poi(poi(@rsp+38)));
			
			
			printf("\n\n===========================================================================\n");
			printf("Packet buffer is: %s\n", dq(poi(poi(@rsp+38))+8));
			
			for (i = 0; i < buffer_len;i++) {
				printf(" %x", db(poi(poi(poi(@rsp+38))+8)+i));
			}

		}
	}
}